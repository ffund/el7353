# README #

This is a repository for Lab5.

### Background ###
Here we aim at verifying the results for M/D/1 queue.
We know that the average queue length should be equal to: p.p/2(1-p) where p is equal to average packet inter arrival time divided by average packet processing time.

### Results ###


### Run my experiment Using GENI: ###

1. Create a 3-node architecture similar to that of Lab2, with a client, a router and a server. (RSPEC file is added to this repo for your reference)

2. Connect to the router and identify the network interface that connects it to the server. (If you've used the RSPEC file to create the architecture it would be eth1.)

3. Make sure you have DITG installed on both the client and the server, and on the server run DITG listener: $ ITGRecv

4. I've written scripts to automate packet generation and collection according to the specification of my experiment (5 Lambda values, 11 trials for each) you are welcome to use these scripts to reproduce the experiment results:

- run_me_at_client: as the name suggests should be run at the client, this will run ITGSend for specified values of lambda 11 times with different random seeds. (Copy the file to the client using git/sftp/scp and run the bash script: ./run_me_at_client)

- run_me_at_router: should be run at the router, run it 5-10 seconds after the client script. You should also edit this file specify the interface that connects the router to the server. at line 3 change IFACE by default it is set to eth1.  (Copy the file to the client using git/sftp/scp and run the bash script: ./run_me_at_router)

This will monitor the queue and generate an output in this format:

lambda1: AVG_Q_SIZE_TRIAL_1 AVG_Q_SIZE_TRIAL_2 ... AVG_Q_SIZE_TRIAL_11

lambda2: AVG_Q_SIZE_TRIAL_1 AVG_Q_SIZE_TRIAL_2 ... AVG_Q_SIZE_TRIAL_11

...

lambda3: AVG_Q_SIZE_TRIAL_1 AVG_Q_SIZE_TRIAL_2 ... AVG_Q_SIZE_TRIAL_11



### Run my experiment Using NS2: ###