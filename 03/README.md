# Background
In this experiment, you are going to measure the average queue length of M/D/1 queue as 
queue occupancy increases.The system topology is like:
![](http://i4.tietuku.cn/1a58ccda97542e11.jpg)

We know that the average queue length of M/D/1 queue is given by

E(N)=(1-0.5ρ)·ρ/(1-ρ)

where 

ρ=λ/μ

and λ and μ are the rate at which packets arrive at the 
queue (in packets/second) and the rate at which packets may be served by the queue (in 
packets/second).

# Results
The average queue length is the quantity we will be measuring in this lab assignment.
You will create a plot similar to this one:
![](http://i4.tietuku.cn/7d5ac6dad6d8bb83.jpg)

As we can see, the expect result is that, as ρ increases, the average queue occupancy E(N) of 
M/D/1 queue increases correspondingly.

You will also measure how well the realization of the M/D/1 queue matches its analytical
properties, with a plot of values of ρ in testbed and simulation versus analytical values.
The plot is like:
![](http://i4.tietuku.cn/0d1e50215f6c9630.jpg)

# Instructions
## D-ITG Testbed Experiment
1. To run experiment on the GENI testbed, we need to install D-ITG on the client node and 
the server node. Log into each node and run the following commands:

  `sudo apt-get update`
   
  `sudo apt-get install d-itg`

2. Set up the router node to pass traffic with a queue rate at 1Mbps. Run the following 
command on the router node.

 `sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600`

3. Start `ITGRecv` on the server node.
4. Start ITGSend on the client node with fixed packet size of 512B and varying arrival rate λ. 
We will run experiments for the following values of λ: 225, 200, 175, 150, 125. And for each 
value of λ, we will run 5 experiments to obtain the result in average. The command is set to 
run for a duration of 60 seconds.

 `ITGSend –a server –l sender.log –x receiver.log –E λ –c 512 –t 60000 –T UDP`
 
5. When ITGSend ends, use ITGDec on the server node to decode the receiver node and obtain 
the value of arrival rate λ.

 `ITGDec receiver.log`
 
6. As the packet size is fixed, we obtain a constant service rate

 μ=queue rate/(8·packet size)=100000/(8·512)=244.140625 pkt/s

 For each experiment, the queue occupancy ρ=λ/μ. 

 Compute the average queue occupancy for M/D/1 queue using ρ: E(N)=(1-0.5ρ)·ρ/(1-ρ)
 
## ns2 Simulation

1. To run ns2 simulation, we designate the client node as the ns2 node, and install ns2 on 
the client node. Run the following commands on the client node:
 
 `sudo apt-get update`

 `sudo apt-get install ns2`
2. Create md1.tcl by modifying the original mm1.tcl in lab 2.

 a) On the client node, run the following command to download the original script.

 `wget https://www-sop.inria.fr/members/Eitan.Altman/COURS-NS/TCL+PERL/mm1.tcl`
 
 b) Change the default maximum packet size into 100000.
 
 Add the line `$src ser packetSize_ 100000` after the command `set src [new Agent/UDP]` .
 
 c) Alter the exponentially distributed packet size into fixed packet size 512B.

 Change the original command 

 `set pktSize [new RandomVariable/Exponential]`
 
 `$pktSize set avg_ [expr 100000.0/(8*$mu)]`

 into

 `set pktSize [new RandomVariable/Constant]`
 
 `$pktSize set val_ 512`
 
 d) Add a random number generator for the interarrival time.

 Add a few lines immediately after `set ns [new Simulator]` :

 `set rep [lindex $argv 0]`
 
 `set rng1 [new RNG]`
 
 `for {set I 1} {$i<$rep} {incr i} {`
 
 `$rng1 next-substream`

 `}`
 
 Add the line `$InterArrivalTime use-rng $rng1` just after `set InterArrivalTime 
 [new RandomVariable/Exponential]` .

 e) Rename the script.
 
 `mv mm1.tcl md1.tcl`
3. We will run experiments for the following values of λ: 225, 200, 175, 150, 125.
And for each value of λ, we will run 5 experiments with different values for seed i: 
10, 20, 30, 40, 50. The command run on the client node is:

 `ns md1.tcl SEED-VALUE`
 
4. For each experiment, we use awk to obtain the average values of time and number 
of arrival packets from the output file qm.out: 

 `cat qm.out | awk ‘{ sum += $1 } END { if (NR > 0) print sum / NR}’ //time`
 
 `cat qm.out | awk ‘{ sum += $6 } END { if (NR > 0) print sum / NR}’ //arrival packets`
 
 And then compute the arrival rate:
 
 λ=number of arrival packets/time
 
5. As the packet size is fixed, we obtain a constant service rate

 μ=queue rate/(8·packet size)=100000/(8·512)=244.140625 pkt/s

 For each experiment, the queue occupancy ρ=λ/μ. 
 
 Compute the average queue occupancy for M/D/1 queue using ρ: 
 E(N)=(1-0.5ρ)·ρ/(1-ρ)
6. In the end, we use the analytical values of the average M/D/1 queue occupancy E(N) 
to compare with results obtained from D-ITG testbed experiment and ns2 simulation to 
observe their relationship.