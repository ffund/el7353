
# for finding the queue length in DITG experiment from queuemonitor.sh script

cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }'



# for finding the queue length in ns2 experiment from qm.out

cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }' 


# for finding the arrival rate 

cat qm.out | awk  '{ sum += $1 } {p +=$6 }  END { if (p > 0) print p / sum }'

# for finding simulation service rate

cat out.tr | awk  '{ sum += $6 } END { if (NR > 0) print sum / NR }'


