# README #

Follow the below instructions to simulate an M/D/1 queue using DITG and NS2.

## Instructions ##

We will be using GENI with server-router-client (dumbbell model) setup.
To generate traffic with exponentially distributed inter-arrival times and fixed packet size, we will use the D-ITG traffic generator.

You will need to install D-ITG on your client and server nodes. Log in to each and run

~~~~
sudo apt-get update # refresh local information about software repositories
sudo apt-get install d-itg # install D-ITG software package
~~~~


Next we install ns2 on one of the machines using the below commands

~~~~
sudo apt-get update # refresh local information about software repositories
sudo apt-get install ns2 # install ns2 network simulator
~~~~




## Queue Monitor Setup ##

We will set the traffic service rate of the queue to 1mpbs , you can do that using the below command

~~~~
sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600
~~~~

Next to run the script `queuemonitor.sh` (check the source), make it an executable script using the command 

~~~~
chmod a+x queuemonitor.sh
~~~~

You can run the script using the command

~~~~
/queuemonitor.sh eth2 40 0.1 | tee router.txt
~~~~

This monitors the queue for 40 seconds with 0.1 interval on eth2 interface.

## DITG setup ##

* Server node will run as Receiver and Client node will be the Sender.
* On the Server node, run the command `ITGRecv` to listen to the traffic from client node.
* Start sending the packets from client node with packets arrival at exponential time and with constant packet size 512 B by running the command 

~~~~
ITGSend -a server -l sender.log -x receiver.log -E 225 -e 512 -t 85000 -T UDP
~~~~
* Once the client starts sending packets to the server, we can start monitoring the traffic using tcpdump on Router node to confirm  exponential inter-arrival times of packets.
Use the below command for monitoring the packets 

~~~~
sudo tcpdump -i eth1 -n -ttt udp and port 8999 -w output
~~~~

* Process the output file to verify that packets are arriving at an exponential distribution rate.
* Once we have confirmed exponential arrival rate at the queue, we shall run the queue monitor script to find the average queue size.Use the below command to extract queue length from the output file `router.txt`

~~~~
cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }'
~~~~

* The simulation time for the sender can be fixed at 85 seconds for different values of lambda, once the simulation is finished , we can use the command `ITGDec` on the log files to find the arrival rate and also confirm the average packet size.
* We repeat this experiment for five times for each value of lambda (225,200,175,150) , to ensure and verify the stochastic randomness of our experiment.
* We then document the results of the experiment and calculate the respective utilization factors and queue length (both theoretical and experimental) and plot the graph for further analysis.


## NS2 setup ##

* Run the script `md1.tcl` using the below command

~~~~
ns md1.tcl
~~~~

* For the ensuring randomness in the simulations , we will use a "rep" for randomizing packet arrival rates. Use the below command for obtaining random results for different values of "rep".

~~~~
ns md1.tcl 55
~~~~

We have used rep values 35-55 with an incrementing interval of 5 for different values of lambda λ.
* The output of the tcl script generates two files `out.tr` and `qm.out`
* We can extract the average queue length and packet arrival rate λ from `qm.out` , use the following commands

Packet Arrival Rate λ 

~~~~
cat qm.out | awk  '{ t += $1 } { p += $6 } END { if (t > 0) print p / t }' 
~~~~

Average Queue Length

~~~~
cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }' 
~~~~

* From the trace file `out.tr` we can extract average packet size in the simulation , use the below command

~~~~
cat out.tr | awk  '{ sum += $6 } END { if (NR > 0) print sum / NR }' 
~~~~

* Repeat the experiment for different values of "rep" for each value of lambda λ (225,200,175,150).
* Plot a graph for Analytical , Simulation and Test-bed results on Average Queue Length Vs Utilization ρ and Experimental ρ Vs Ideal ρ