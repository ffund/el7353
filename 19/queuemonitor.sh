#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Usage: $0 interface duration interval"
    echo "where 'interface' is the name of the interface on which the queue"
    echo "running (e.g., eth2), 'duration' is the total runtime (in seconds),"
    echo "and 'interval' is the time between measurements (in seconds)"
    exit 1
fi

interface=$1
duration=$2
interval=$3

end=$((SECONDS+$duration))

while [ $SECONDS -lt $end ]
do
    echo -n "$(date +%s.%N) "
    echo $(tc -p -s -d qdisc show dev $interface)
    sleep $interval
done
