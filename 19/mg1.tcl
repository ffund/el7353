set ns [new Simulator]

set rep [lindex $argv 0]

set rng1 [new RNG]
set rng2 [new RNG]

for {set i 1} {$i<$rep} {incr i} {
        $rng1 next-substream;
        $rng2 next-substream
}

set tf [open out.tr w] 
$ns trace-all $tf

set lambda [lindex $argv 1]
set x [lindex $argv 2]

set n1 [$ns node]
set n2 [$ns node]
# Since packet sizes will be rounded to an integer
# number of bytes, we should have large packets and
# to have small rounding errors, and so we take large bandwidth
set link [$ns simplex-link $n1 $n2 1mb 0ms DropTail]
$ns queue-limit $n1 $n2 100000

# generate random interarrival times and packet sizes
set InterArrivalTime [new RandomVariable/Exponential]
$InterArrivalTime use-rng $rng1
$InterArrivalTime set avg_ [expr 1/$lambda]
if {$x == 0.0 || $x == 1.0} {
    set pktSize [new RandomVariable/Uniform]
    $pktSize use-rng $rng2
    $pktSize set min_ [expr 512-$x*512]
    $pktSize set max_ [expr 512+$x*512]
} else {
    set pktSize [new RandomVariable/Exponential]
    $pktSize use-rng $rng2
    $pktSize set avg_ 512
}

set src [new Agent/UDP]
$src set packetSize_ 100000
$ns attach-agent $n1 $src

# queue monitoring
set qmon [$ns monitor-queue $n1 $n2 [open qm.out w] 0.1]
$link queue-sample-timeout

proc finish {} {
    global ns tf
    $ns flush-trace 
    close $tf 
    exit 0 
} 

proc sendpacket {} {
    global ns src InterArrivalTime pktSize 
    set time [$ns now]
    $ns at [expr $time + [$InterArrivalTime value]] "sendpacket"
    set bytes [expr round ([$pktSize value])]
    $src send $bytes
}

set sink [new Agent/Null]
$ns attach-agent $n2 $sink
$ns connect $src $sink
$ns at 0.0001 "sendpacket"
$ns at 30
$ns at 60.0 "finish"

$ns run
