import numpy as np
import matplotlib.pyplot as plt

def plotstatic(i, j):
    x = np.linspace(j+0.002*i, j+0.002*i)
    if i == 0:
        color = 'b'
        dot = 'bo'
    elif i == 1:
        color = 'g'
        dot = 'go'
    elif i == 2:
        color = 'r'
        dot = 'ro'
    if j == 0.9216:
        y = np.linspace(np.min(data[i*25:5+i*25]), np.max(data[i*25:5+i*25]))
        plt.plot(x, y, color)
        plt.plot(j+0.002*i, np.mean(data[i*25:5+i*25]), dot)
    elif j == 0.82:
        y = np.linspace(np.min(data[5+i*25:10+i*25]), np.max(data[5+i*25:10+i*25]))
        plt.plot(x, y, color) 
        plt.plot(j+0.002*i, np.mean(data[5+i*25:10+i*25]), dot)              
    elif j == 0.7168:
        y = np.linspace(np.min(data[10+i*25:15+i*25]), np.max(data[10+i*25:15+i*25]))
        plt.plot(x, y, color) 
        plt.plot(j+0.002*i, np.mean(data[10+i*25:15+i*25]), dot)                      
    elif j == 0.6144:
        y = np.linspace(np.min(data[15+i*25:20+i*25]), np.max(data[15+i*25:20+i*25]))
        plt.plot(x, y, color)
        plt.plot(j+0.002*i, np.mean(data[15+i*25:20+i*25]), dot)
    elif j == 0.512:
        y = np.linspace(np.min(data[20+i*25:25+i*25]), np.max(data[20+i*25:25+i*25]))
        plt.plot(x, y, color)        
        plt.plot(j+0.002*i, np.mean(data[20+i*25:25+i*25]), dot)

fig1 = plt.figure(1)
rho = np.linspace(0.5, 0.95, 100)
l1 = plt.plot(rho, rho**2/(1-rho), 'r', label = r'$\sigma^2$ = 1/$\mu^2$')
l2 = plt.plot(rho, rho/(1-rho)*(1-rho/3)-rho, 'g', label = r'$\sigma^2$ = 1/3$\mu^2$')
l3 = plt.plot(rho, rho/(1-rho)*(1-rho/2)-rho, 'b', label = r'$\sigma^2$ = 0')
plt.legend(loc = 'upper left')
plt.grid()
plt.xlim(0.5, 1.0)
plt.ylim(0.0, 20.0)
plt.xlabel(r'Ideal $\rho$')
plt.ylabel('Average queue length')
data = np.loadtxt('nsqueue.txt')
idealrho = [0.9216, 0.82, 0.7168, 0.6144, 0.512]
# 0.9216 0.82 0.7168 0.6144 0.512
for i in [0, 1, 2]:
    for j in idealrho:
        plotstatic(i, j)          
plt.savefig('ns.eps')
fig2 = plt.figure(2)
l1 = plt.plot(rho, rho**2/(1-rho), 'r', label = r'$\sigma^2$ = 1/$\mu^2$')
l2 = plt.plot(rho, rho/(1-rho)*(1-rho/3)-rho, 'g', label = r'$\sigma^2$ = 1/3$\mu^2$')
l3 = plt.plot(rho, rho/(1-rho)*(1-rho/2)-rho, 'b', label = r'$\sigma^2$ = 0')
plt.legend(loc = 'upper left')
plt.grid()
plt.xlim(0.5, 1.0)
plt.ylim(0.0, 20.0)
plt.xlabel(r'Ideal $\rho$')
plt.ylabel('Average queue length')
data = np.loadtxt('queue.txt')
idealrho = [0.9216, 0.82, 0.7168, 0.6144, 0.512]
for i in [0, 1, 2]:
    for j in idealrho:
        plotstatic(i, j)          
plt.savefig('geni.eps')         
