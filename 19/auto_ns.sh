#!/bin/bash

test=(225.0 200.0 175.0 150.0 125.0)
std=(0.0 1.0 2.0)
count=(1 2 3 4 5)

for std in ${std[*]}
do
    for lambda in ${test[*]}
    do
        for i in ${count[*]}
        do
            echo "exp $std $lambda $i"
            ns mg1.tcl $i $lambda $std
            cat qm.out | awk '{ sum += $5 } END { if (NR > 0) print sum / NR }' >> nsqueue.txt
            ap=($(cat qm.out|awk '{print $6}'))
            size=${#ap[@]}
            for((j=0;j<$size-1;j++))
            do
                echo "${ap[j+1]}-${ap[j]}"|bc >> ap.txt
            done
            cat ap.txt | awk '{ sum1 += $1 } END { print sum1 / NR / 0.1 }' >> nslambda.txt
            > ap.txt
            cat qm.out | tail -1 | awk '{ print 1000000 / $10 / 8 * $7 }' >> nsmu.txt
        done
    done
done