# README #

This README is divided into three sections. The first section is to introduce the background of this experiment and the second is how to run my experiment and generate the result that I want to report step by step. The last section is the analysis of my results.

### Background ###
There are altogether two results that we want to validate in this experiment.

* In the M/M/1 queue, the mean delay in the system is (E(N))/λ=(1/μ)/(1-ρ) using the Little’s Formula
* Having a single processor that is k times faster is better than distribute jobs over k slower processors


### Procedures of the experiment based on simulation ###

* Develope the M/M/1 script in ns2

Use the modified "mm1.tcl" scrippt to run the simulation version.

* Use awk script to get end-to-end delays from ns-2 tracefiles

Run the "delay.awk" on the trace file "out.tr" and the save the results to the textfile named "time..txt". Run


```
awk -f delay.awk < out.tr > time.txt
```

* Use the "awk" to commpute the average value of the second column (end-t-end delays). Run

```
cat time.txt | awk '{ sum += $2 } END { if (NR > 0) print sum / NR }'
```

* Run the above steps for the varying λ and μ.


### Procedures of the experiment based on testbed ###

* Create the three-node topolog of a "client, router and server" model to develop a M/M/1 queue.
* Use "ping" to measure the delay between client and server when the network is free. In the client node, run

```
ping server
```

* Use D-ITG to generate traffic between client and server. On the receiver(server) node, run

```
ITGRecv
```

* Leave that running and on the sender(client) node, run

```
ITGSend -a server -l sender.log -x receiver.log -E 200 -e 512  -t 180000 -T UDP
```

This will send UDP traffic to the server with a packet interarrival time that is exponentially distributed with mean 200 packets/second, packet size that is exponentially distributed with mean 512 bytes, and will save the log files at the client node and server node with file names "sender.log" and "receiver.log" respectively. The transmission time is set to 3 minutes to make sure enough time for "ping" to measure the delay.

* Open a new client window when the D-ITG is working and run

```
ping server
```

To make the data convincible, I use the first 100 ICMP packets to measure the average value of delay. Subtract the average value of delay when network is free from that when network is busy and then we can get the average value of (Tq + Ts).

* Run the above steps for the varying λ and μ.

### Data analysis of results ###

* When we set λ=225 and μ=244.14 on the testbed platform, the average value of the five trials is 55.465 ms which matches the ideal E(T)=52.245 ms well. And when we change the two parameters to twice as before, the measured results has also changed to 25.483 ms which matches the second result that we want to validate.
* The figure plotted using the data in ns2 simulator is shown here:

![ns2.png](http://i.imgur.com/SOgg79a.png)

From the figure we can see that the results of ns2 match the ideal ones very well which meet our requirements.