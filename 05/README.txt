To run the experiment on network simulator-2 and GENI please go through different folder.
it contains three folders:
1: GENI_experiment : contains scripts and README files for GENI Experiment.
2: ns-2 Experiment : contains scripts and README files for ns-2 Experiment.
3: MatlabScripts   : contains MATLAB Scripts to plot results obtained in GENI testbed experiment and ns-2 experiment.
