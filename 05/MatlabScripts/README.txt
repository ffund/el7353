This readme file is for visual representation of results 
obtained after GENI and Network Simulator-2. You need 
certain files to run scripts inside this folder:
1: genilambda.txt :- This is the file containing arrival rates in GENI testbed for M/D/1 queue.
                     This file contain arrival rate in Kbps. Each column contains the information
                     for different arrival rate and each row contains the information of result obtained
		     in different seed value.
2: nslambda.txt   :- This is the file containing arrival rates in ns-2 simulation for M/D/1 queue. 
		     This file contain arrival rate in Kbps. Each column contains the information 
		     for different arrival rate and each row contains the information of result obtained
		     in different seed value.
3: geniQueue.txt  :- This is the file containing queue length in GENI testbed for M/D/1 queue. This 
		     file contain queue length in packets. Each column contains the information for 
		     different arrival rate and each row contains the information of result obtained
		     in different seed value.
4: nsQueue.txt	  :- This is the file containing queue length in ns-2 simulation for M/D/1 queue. 
		     This file contain queue length in packets. Each column contains the information
		     for different arrival rate and each row contains the information of result obtained
		     in different seed value.
5: geniQueueMM1.txt:-This is the file containing queue length in GENI testbed for M/M/1 queue. This
		     file contain queue length in packets. Each column contains the information for 
		     different arrival rate and each row contains the information of result obtained
		     in different seed value.  	
6: script.m	   :-This MATLAB code plots the results for M/D/1 queue for analytical study, ns-2 and 
		     GENI testbed simulation. Please go through code to understand. I have included the
		     example for reference.
7: mm1vsmd1.m	   :-This MATLAB code plots the results for M/D/1 queue and M/M/1 queue. Please go 
                     through code to understand it. 
