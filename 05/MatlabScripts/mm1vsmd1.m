clc; clear all;
geniQueLen = load('geniQueue.txt');
geniQueLenMM1 = load('geniQueueMM1.txt');
meanGENIQueLen = mean(geniQueLen);
meanGENIQueLenMM1 = mean(geniQueLenMM1);
confidenceIntervalQueueGENI = 1.96*std(geniQueLen)/sqrt(5); % confidence interval for GENI queue M/D/1
confidenceIntervalQueueGENIMM1 = 1.96*std(geniQueLenMM1)/sqrt(5); % confidence interval for GENI queue M/M/1
pointsofInterest =[125/250 150/250 175/250 200/250 225/250];
%%%%%%%%%%%%%%%%%%Queue Plots%%%%%%%%%%%%%%
figure(1)
theorUtilityFactor = 0.5:0.01:1;
theoriticalQueue = (theorUtilityFactor.*theorUtilityFactor)./(2*(1-theorUtilityFactor));
theoriticalQueueMM1 = theorUtilityFactor./(1-theorUtilityFactor);
plot(theorUtilityFactor,theoriticalQueue,'-b')
hold on; grid on
plot(theorUtilityFactor,theoriticalQueueMM1,'-k')
errorbar(pointsofInterest,meanGENIQueLen,confidenceIntervalQueueGENI,'r.','LineWidth',0.5,'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10)
errorbar(pointsofInterest,meanGENIQueLenMM1,confidenceIntervalQueueGENIMM1,'m.','LineWidth',0.5,'MarkerEdgeColor','m','MarkerFaceColor','m','MarkerSize',10)
xlim([0.45 1])
legend('Theoretical Result: M/D/1','Theoretical Result: M/M/1','GENI Result: M/D/1','GENI Result: M/M/1')
xlabel('Theoritical Utility Factor') % x-axis label
ylabel('Queue Length [Packets]') % y-axis label
hold off