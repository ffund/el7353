clc; clear all;
nsQueLen = load('nsQueue.txt'); % loading text file containing ns-2 relaizaion with each run and with each utility factor
nsUtilizationFactor =load('nslambda.txt'); % loading realized utility factor in ns-2
geniQueLen = load('geniQueue.txt'); % loading text file containing GENI relaizaion with each run and with each utility factor
geniUtilizationFactor = load('genilambda.txt'); % loading realized utility factor in GENI, this contains information of bit transmittied 
serviceRate = 1024; % service rate is fized as 1024Kbps
nsUtilizationFactor = nsUtilizationFactor/1024; % finding utlility factor by dividing incoming rate/outgoing rate
meanNSUtilizationFactor = mean(nsUtilizationFactor); % finding mean of utility facor
geniUtilizationFactor = geniUtilizationFactor/1024; % finding utlility factor by dividing incoming rate/outgoing rate
meanGENIUtilizationFactor = mean(geniUtilizationFactor); % finding mean of utility facor
meanNSQueLen = mean(nsQueLen); % mean queuelength in ns 2
meanGENIQueLen = mean(geniQueLen); % mean queuelength in GENI
confidenceIntervalQueueNS = 1.96*std(nsQueLen)/sqrt(5); % confidence interval for ns2 queue
confidenceIntervalQueueGENI = 1.96*std(nsQueLen)/sqrt(5); % confidence interval for GENI queue
confidenceIntervalUFNS = 1.96*std(nsUtilizationFactor)/sqrt(5); % confidence interval for ns2 UF
confidenceIntervalUFGENI = 1.96*std(geniUtilizationFactor)/sqrt(5); % confidence interval for GENI UF
pointsofInterest =[125/250 150/250 175/250 200/250 225/250]; % ideal utilization factor
%%%%%%%%%%%%
figure(1)
plot(pointsofInterest, pointsofInterest,'-b.','LineWidth',0.5,'MarkerEdgeColor','b','MarkerFaceColor','b','MarkerSize',10)
hold on; grid on
errorbar(pointsofInterest,meanNSUtilizationFactor,confidenceIntervalUFNS,'r.','LineWidth',0.5,'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10)
errorbar(pointsofInterest,meanGENIUtilizationFactor,confidenceIntervalUFGENI,'k.','LineWidth',0.5,'MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',10)
xlim([0.45 0.95])
xlabel('Measured Utility Factor') % x-axis label
ylabel('Theoretical Utility Factor') % y-axis label
legend('Theoretical Result','ns-2 Result','GENI Results')
hold off
%%%%%%%%%%%%%%%%%%Queue Plots%%%%%%%%%%%%%%
figure(2)
theorUtilityFactor = 0.5:0.01:1;
theoriticalQueue = (theorUtilityFactor.*theorUtilityFactor)./(2*(1-theorUtilityFactor));
fewTheoQueLen = pointsofInterest +(pointsofInterest.*pointsofInterest)./(2*(1-pointsofInterest));
plot(theorUtilityFactor,theoriticalQueue,'-b')
hold on; grid on
errorbar(pointsofInterest,meanNSQueLen,confidenceIntervalQueueNS,'r.','LineWidth',0.5,'MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10)
errorbar(pointsofInterest,meanGENIQueLen,confidenceIntervalQueueGENI,'k.','LineWidth',0.5,'MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',10)
xlim([0.45 1])
legend('Theoretical Result','ns-2 Result','GENI Results')
xlabel('Theoretical Utility Factor') % x-axis label
ylabel('Queue Length [Packets]') % y-axis label
hold off