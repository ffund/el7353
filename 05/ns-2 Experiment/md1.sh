max_lambda=225

function execute {

while [[ $lambda -le $max_lambda ]];
do 

	while [[ $run -le 5 ]];
	do
		temp=0
		outfile=tracefile_"$lambda"_"$run".tr 
		qmonfile=queueMonitor_"$lambda"_"$run".out
		csvout=csv_"$lambda"_"$run".csv
		quelen=meanQueueLength.txt
		echo "Executing run number $run for $lambda"
		temp_lambda=$lambda.0
		ns mm1.tcl $seed $outfile $qmonfile $temp_lambda $mu
		run=$(($run+1))
		seed=$(($seed+10)) 
		cat $qmonfile | awk '{sum+=$5} END {if (NR > 0) print sum / NR}' >> $quelen 
		temp=$temp+$quelen
		grep -r "r" $outfile > test.txt
		awk -F "[: ]" '{print $2", "$6}' test.txt > $csvout
		rm test.txt
	done
run=1
lambda=$(($lambda+25))
echo "Value for lambda is $lambda"
done

}

seed=236
run=1
lambda=125
mu=250.0
execute

