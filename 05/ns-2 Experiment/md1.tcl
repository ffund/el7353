set ns [new Simulator]

set rep [lindex $argv 0]

set rng1 [new RNG]
set rng2 [new RNG]

for {set i 1} {$i<$rep} {incr i} {
	$rng1 next-substream;
	$rng2 next-substream;
}


set flowtrace [lindex $argv 1]
set queuemonitor [lindex $argv 2]

set tf [open $flowtrace w] 
$ns trace-all $tf

set lambda [lindex $argv 3]
set mu     [lindex $argv 4]

set n1 [$ns node]
set n2 [$ns node]
# Since packet sizes will be rounded to an integer
# number of bytes, we should have large packets and
# to have small rounding errors, and so we take large bandwidth
set link [$ns simplex-link $n1 $n2 1024kb 0ms DropTail]
$ns queue-limit $n1 $n2 100000000

# generate random interarrival times and packet sizes
set InterArrivalTime [new RandomVariable/Exponential]
$InterArrivalTime use-rng $rng1
$InterArrivalTime set avg_ [expr 1/$lambda]
#set pktSize [new RandomVariable/Exponential]
set pktSize $mu

set src [new Agent/UDP]
$src set packetSize_ 1000000
$ns attach-agent $n1 $src

# queue monitoring
set qmon [$ns monitor-queue $n1 $n2 [open $queuemonitor w] 0.1]
$link queue-sample-timeout

proc finish {} {
    global ns tf
    $ns flush-trace 
    close $tf 
    exit 0 
} 

proc sendpacket {} {
    global ns src InterArrivalTime pktSize 
    set time [$ns now]
    $ns at [expr $time + [$InterArrivalTime value]] "sendpacket"
    set bytes $pktSize
    $src send $bytes
}

set sink [new Agent/Null]
$ns attach-agent $n2 $sink
$ns connect $src $sink
$ns at 0.0001 "sendpacket"
$ns at 60.0 "finish"

$ns run

