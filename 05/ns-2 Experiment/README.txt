This readme file contains about network simulator--2 simulations for M/D/1 queue.
This folder contains following files:
1: md1.tcl: arguments can be passed from terminal. We have kept packet length as fixed and service rate as 1 Mbps.
2: md1.sh : provides the arguments to md1.tcl for different iteration and saving the output in desired form.
3: counting.m : counts the total number of packets transmitted in a given runtime.
This will generate .csv files. We use these file to measure arrival rate in each experiment. Please check the code 
for detailed description. Please save the result in a text file for future reference. 
