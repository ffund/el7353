## Instructions

You have been assigned several submissions to review.

* Log in to [the review site](http://141.217.114.138/hotcrp/) with your email address and password (distributed to you by email.)
* Make a note of the submission numbers of your assigned reviews.
* Match the submission numbers of your assigned reviews to the folder numbers in the [Source](https://bitbucket.org/ffund/el7353/src) section of this repository.

Use the contents of the folder associated with your assigned review to reproduce the author's experiment.

You may find it helpful to get a copy of this entire repository, either on your own laptop or on a GENI VM. You can clone this repository with

```
git clone http://ffund@bitbucket.org/ffund/el7353.git
```