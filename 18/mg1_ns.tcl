set ns [new Simulator]

#Added
set rep [lindex $argv 0]
set div [lindex $argv 1]
set str [lindex $argv 2]

set rng1 [new RNG]
set rng2 [new RNG]

for {set i 1} {$i<$rep} {incr i} {
        $rng1 next-substream;
        $rng2 next-substream
}
# Till here

set tra "out"
set qmo "qm"

append tra $str ".tr"
append qmo $str ".out"
#$puts $tra
set tf [open $tra w] 
$ns trace-all $tf

set lambda 46.0
set mPac 1024.0
set mu    [expr (1000000/$mPac)/8]
#set duration 100.0
set n1 [$ns node]
set n2 [$ns node]

# Since packet sizes will be rounded to an integer
# number of bytes, we should have large packets and
# to have small rounding errors, and so we take large bandwidth
set link [$ns simplex-link $n1 $n2 500Kb 0ms DropTail]
$ns queue-limit $n1 $n2 100000

# generate random interarrival times and packet sizes
set InterArrivalTime [new RandomVariable/Exponential]
$InterArrivalTime use-rng $rng1
$InterArrivalTime set avg_ [expr 1/$lambda]

set pktSize [new RandomVariable/Uniform]
$pktSize use-rng $rng2
$pktSize set min_ [expr $mPac-$div]
$pktSize set max_ [expr $mPac+$div]

set src [new Agent/UDP]
#Added
$src set packetSize_ 600000 
$ns attach-agent $n1 $src

# queue monitoring
set qmon [$ns monitor-queue $n1 $n2 [open $qmo w] 0.01]
$link queue-sample-timeout

proc finish {} {
    global ns tf
    $ns flush-trace 
    close $tf 
    exit 0 
} 

proc sendpacket {} {
    global ns src InterArrivalTime pktSize 
    set time [$ns now]
    $ns at [expr $time + [$InterArrivalTime value]] "sendpacket"
    set bytes [expr round ([$pktSize value])]
    $src send $bytes
}

set sink [new Agent/Null]
$ns attach-agent $n2 $sink
$ns connect $src $sink
$ns at 0.0001 "sendpacket"
$ns at 90.0 "finish"

$ns run

