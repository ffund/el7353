#!/bin/bash

for div in  0 200 400 750 1000
do
for itr in {1..5}
do
str="${div}_${itr}"
cat router_${str}.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print "'"$div"'"" " sum / NR }' >> itg_data.txt
done
done
