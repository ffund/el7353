# README #

### Queue length for M/G/1 queue ###

We will verify the theoretical formula for the average queue length of an M/G/1 queue using ns-2 simulator and D-ITG on the GENI test bed.

### For ns-2 simulation ###

For the ns-2 simulation use the mg1_ns.tcl script available in this repository. The script can be run using the command

    ns mg1_ns.tcl 4.0 200.0 a200

where the first argument is used for the random seed generation, the second gives the spread of the packet size (refer to the report) and the third agreement is a string used to differentiate the traces.

To better automate the process, I have included the script

     runMG.sh 

Run this script in the shell and it generates a file nsdata_1.txt which contains the queue length for various values of  spread of the packet size. No arguments are needed. You may change parameters within this file if you wish so.

### For the test bed ###

At the router set the token buffer queue as

    sudo tc qdisc replace dev eth2 root tbf rate 500kbit limit 200mb burst 32kB peakrate 501kbit mtu 2048

You may have to change *eth2* to the interface connecting your router to the server. Start the ITG server at the VM designated as the server

    ITGRecv

From the client VM start sending uniformly distributed UDP packets as

    ITGSend -a server -l sender.log -x receiver.log -E 46 -u 274 1774 -t 105000 -T UDP

The numbers after the -u specifies the upper and lower limits of the packet sizes. Set the lower limit as 1024-a and the upper limit as 1024+a where a = 200,400,750, 1000. For the case of a=0 use the following command at the client

    ITGSend -a server -l sender.log -x receiver.log -E 46 -c 1024 -t 105000 -T UDP

Use the script 

    queuemonitor.sh 

from Lab 2 for the monitoring the queue at the router. You can serially use the router traces like

    ./queuemonitor.sh eth2 40 0.1 | tee router_200_1.txt

and mark the files serially like router_200_1, router_200_2 etc. for various values of a and use 

    ./writedata.sh

the script is included in the repository to create the file **itg_data.txt** containing the queue length information for the test bed experiment.

### Data files ###
The following data files are included with this repository

* pdf400.txt and pdf750.txt : Capture of packet sizes with ITG to study probability density for a=400 and a=750
* ns400.txt and ns750.txt : Trace of packet sizes with ns-2 to study probability density for a=400 and a=750
* itg_data.txt : Average queue length data for test bed experiment
* nsdata_1.txt : Average queue length data for ns-2 experiment
* plotData.m : MATLAB script parse the date files and plot graphs.