
clear; clc; close all;

data1 = importdata('pdf400.txt');
nsTr1 = importdata('ns400.txt');

Size = data1(:,2);

numPac = nsTr1(2:end,1) - nsTr1(1:end-1,1);
pacSz = nsTr1(2:end,2) - nsTr1(1:end-1,2);
ind = numPac == 0;
numPac(ind) = [];
pacSz(ind) = [];
Sizns = pacSz./numPac;

N = length(Size);

xt = 580:1:1454;
yt = xt <= 1424 & xt >= 624;
yt = yt/(1424-624);

[sz,pos1] = hist(Size,50);
bw1 = pos1(2)-pos1(1);
pdf1 = sz./(bw1*N);
% 
[szNs, posNs] = hist(Sizns,50);
N0 = length(Sizns);
bw1ns = posNs(2)-posNs(1);
pdf2 = szNs./(bw1ns*N0);

figure(1);
plot(xt,yt,'--r','Linewidth',2); hold on;
plot(pos1,pdf1,'-ob','Linewidth',1);
plot(posNs, pdf2,'-<k','Linewidth',1); grid on;
set(gca,'FontSize',16);
xlabel('Packet size (Bytes)');
ylabel('probability dist. fn');
%xlim([32 1024]);
legend('Theoritical','ITG','ns2','Location','NE');

clear;

data2 = importdata('pdf750.txt');
nsTr1 = importdata('ns750.txt');

Size = data2(:,2);


numPac = nsTr1(2:end,1) - nsTr1(1:end-1,1);
pacSz = nsTr1(2:end,2) - nsTr1(1:end-1,2);
ind = numPac == 0;
numPac(ind) = [];
pacSz(ind) = [];
Sizns = pacSz./numPac;

N = length(Size);

xt = 200:1:1800;
yt = xt <= 1774 & xt >= 274;
yt = yt/(1774-274);

[sz,pos1] = hist(Size,50);
bw1 = pos1(2)-pos1(1);
pdf1 = sz./(bw1*N);
err1 = sqrt(sz)./(bw1*N);
% 
[szNs, posNs] = hist(Sizns,50);
N0 = length(Sizns);
bw1ns = posNs(2)-posNs(1);
pdf2 = szNs./(bw1ns*N0);

figure(2);
plot(xt,yt,'--r','Linewidth',2); hold on;
plot(pos1,pdf1,'-ob','Linewidth',1);
plot(posNs, pdf2,'->k','Linewidth',1); grid on;
set(gca,'FontSize',16);
xlabel('Packet size (Bytes)');
ylabel('probability dist. fn');
%xlim([32 1024]);
legend('Theoritical','ITG','ns2','Location','NE');


% Variance study
dataNs = importdata('nsdata_1.txt');
dataItg = importdata('itg_data.txt');
lam = 46;
meanDat = 1024;
mu = 0.5e6/(meanDat*8);
rho = lam/mu;

diff = dataNs(:,1);
qlen = dataNs(:,2);

avQlen = mean(reshape(qlen,5,5));

var = (2*diff*8/0.5e6).^2/12;
var0 = var(1:5:end);

varItg = (2*dataItg(:,1)*8/0.5e6).^2/12;
qlenItg = dataItg(:,2);

avQlenI = mean(reshape(qlenItg,5,5));
var0I = varItg(1:5:end);

varTh = (2*(0:1e3)*8/0.5e6).^2/12;
nqTh = rho^2/(2*(1-rho))*(1+mu^2*varTh);
figure(3);
plot(varTh, nqTh,'-b','Linewidth',2); hold on;
plot(var,qlen,'sr','Linewidth',2); 
plot(var0, avQlen,'->m');
plot(varItg, qlenItg, 'xk', 'Linewidth',2);
plot(var0I,avQlenI ,'-ok');grid on;
set(gca,'FontSize',16);
xlabel('Variance in service time \sigma^2');
ylabel('Length of the queue N_Q (packets)');
legend('Theoritical','ns2 data','ns2 mean','ITG Data','ITG Mean','Location','SE');
xlim([-1e-5 0.90e-4]);


