#!/bin/bash

for div in  0.0 200.0 400.0 750.0 1000.0
do
   for itr in {0..5}
   do
        str="${div}_${itr}"
        ns mg1_ns.tcl ${itr}+${div} $div $str
        cat qm${str}.out | awk  '{ sum += $5 } END { if (NR > 0) print "'"$div"'"" "sum / NR  }' >> nsdata_1.txt
   done
done
