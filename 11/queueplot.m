clc;
 x = [0.51 0.61 0.72 0.82 0.92 0.95];
 y = [0.27 0.48 0.93 1.87 5.29 9.025];
 figure(1);
 clf;
 x1 = [0.5 0.59 0.692 0.79 0.864];
 y1 = [0.04 0.12 0.30 1.06 4.45];
 
 x2 = [0.5 0.6 0.7 0.8 0.9];
 y2 = [0.25 0.45 0.82 1.6 4.05];
 
 plot(x,y);
 hold on;
 stem(x1,y1, '.');
 title('---analytical /... testbed /*** ns2 simulation');
 stem(x2,y2, '*')
 xlabel('ρ');
 ylabel('average queue length');
 