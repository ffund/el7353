#M/D/1 Experiment

##Backgrounds
My experiment is to verify that the average queue length in M/D/1 system has the relationship with utilization factor as


![Imgur](http://i.imgur.com/bo8wLWz.png)

The system is defined as a node who transmits the packets in a fixed rate and the transmitted packets have equal sizes. Packets come to that node have an interarrival time in exponential distribution and if there are some packets being transmitted, the incoming packet will be buffered until all the packets come before have left the node. Packets are sent by another node and the destination is the same node.  

I did the same experiment in D-ITG and in ns2. The metrics in both of them are the same—the average queue length. The parameters are listed in the table below:


![Imgur](http://i.imgur.com/GBiOzuY.png)

This is a single factor() experiment set them to be five values and for each value do five times with different seeds.

##Run my experiment

###GENI
####Preparations
1. Go to [GENI](https://portal.geni.net) and click the "Use Geni" bollom. Then enter your organization's name and click “Continue”.
2. click the “Join Project” button under the “Projects” tab. Choose the project with name “EL7353-NYU” and click “join”.
3. In a linux-based laptap, open a terminal and type 

		ssh-keygen –t rsa 
	to generate the key. Remember the directory.
4. Find the “SSH Keys” at “Your Name” session on the right corner and click it to go into the "SSH Keys" page.
5. At the end of that page, click the “Upload another SSH public key” button and then choose the public key that you’ve just created and then upload that key.
6. On Slices page, click “+ New slice” and then type the slice name. Slice description is not required. After that click the “Create slice” bottom to finish the creating process.
7. To add resources, click on the black "VM" icon and drag it onto the white canvas, then repeat for two more times.
8. Click one VM and type “client” in “Name” field in the left. Repeat for two more times with the other two VM names “router” and “server”.
9. Click near the "client" VM, then click and drag towards the "router" VM. Release when you reach the "router" VM. Do it again between the “router” and the “server”.
10. Click the "Site 1" button and choose an aggregate from the list. The [principles](http://witestlab.poly.edu/~ffund/el7353/1-reserve-resources.html) to choose an aggregate can be found from the session “Choosing an aggregate”. After you choose an aggregate click the “Reserve Resources” bottom and wait until all the VMs are turned to green. Your should have a topology like

![topo](http://i.imgur.com/dokEsoD.png?1)

####Login to VMs
1. At the “Resources” session click the “Details” button at the end of that page to find the information like the username, hostname, port numbers, and IP address for each node or each interface. 
2. run

		ssh username@hostname –p <port> –i <path>/id_rsa

	the path is where your private key locates and id_rsa might need to be changed to your private key name if you have other names. 
3. Repeat the above steps and make sure that you’ve successfully logged in to the client, server and router. You should log in the router in **two** separate terminals since you need them to do different tasks.
4. (This step is optional.) Since you may get a lot of files some nodes, you could create directories to store those files so that it would be easier to find them. Create a new directory in each node and go into that directory with the command `mkdir` and `cd`

####Find important informations for each node
On each node, get the information like which interface is connected to another VM by command

		ifconfig
	
to show the useful information of each interface. For example, you already know two IP addresses for different interfaces in VM router from preceeding steps, so now you need to know which interface (like “eth1” or “eth2”) corresponds to a certain IP address and based on the link information showed in GENI you should get the specific information for the topology and draw it like
![link](http://i.imgur.com/saDN3kz.png)

####Upload the queue monitor file
Open another terminal without logging in any VM, run
 
		scp -P <port> <path>/queuemonitor.sh  username@hostname:~/path
to upload the queue monitor file on router. Obviously, the port number is in accordance with what you’ve used to log in the router and <path> is where your monitor file locates on your local machine. ~/path can be changed to other locations where you want to store that script.

####Install D-ITG
On both the client and server VMS, run

		sudo apt-get update
		sudo apt-get install d-itg

####Set the queue
On the router, run the command

		sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600

you may change the interface eth2 to your actual interface that connects to the server.

####Send packets and collect data
1.	Run command

		ITGRecv

	on the server to start the ITG receiver.
2.	On the router, run

		sudo tcpdump -i eth1 -n -ttt udp and port 8999 > arv.csv

	where eth1 is the interface of the router which connect with the client, arv.csv is the file name which you’ll copy to your local machine later.
3.	On the client, run

		ITGSend -s 0.1 -a server -l sender.log -x receiver.log -E 50 -c 512 -t 85000 -T UDP
	
	where 0.1 is the seed, sender.log and receiver.log are the log files which provide you useful information to analysis in later steps. The receiver.log is stored on the server VM while the sender.log is on the client VM, -E 50 is to let the sender send the packet with arrival rate of 50 packets per second, -c 512 specifies the packets size, which is constant, -t 85000 is the experiment duration in milliseconds, -T UDP says it send the UDP packets.
4. After about five seconds, run
	
		./queuemonitor.sh eth2 60 0.1 | tee router.txt
	on the router in another terminal, so that you could get the file router.txt with records for every 0.1 seconds and totally 60 seconds. Here, eth2 is the interface which connect with the server.
5.	When the ITGSend on client ends, stop with `ctr C`
on server and router in terminial that is running the tcpdump command.
6.	On the receiver, run
	
		ITGDec receiver.log 
	
	to show the information about the traffic flow. Find the values in **"Average packet rate"**, **"Bytes received"** and **"Total packets"** and record them.
7.	On the router, run

		cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }'
	
	the output is the average queue length. Record that number.
	
	Then run
		
		awk -F "[: ]" '{print $3}' arv.csv > time.csv
	to extract the third column of the file and then redirect that to a new file.
	
	On your local machine, run 

		scp -P <Port> username@hostname:path/time.csv ./
	to copy that file you’ve just made to your local machine for later analysis.


####Repeat the experiments
Repeat the step **"Send packets and collect data"** for four more times. On client, you should modify the command

		ITGSend –s 0.1 -a server -l sender.log -x receiver.log -E 50 -c 512 -t 85000 -T UDP
with different seeds (0.2,0.3,0.4,0.5) like `-s 0.2`.
Record the values **“average queue length”**, **“Average packet rate”**, **“Bytes received”**, **“Total packets”** and **“Variance of packet length”** for each seed. You may also have the same kind of files with different names in each repeated experiments so as to not get confused when recording the values and in later analysis.


####Repeat with different parameters
Now you need to repeat both the step **"Send packets and collect data"** and **"Repeat the experiments"** with different `-E <lambda>`. Change the value of <lambda> to 100, 150, 200, 230 and each with different seeds(0.1, 0.2, 0.3, 0.4, 0.5) for five times.

After you've done that, you can run

		sudo tc qdisc del dev eth2 root

to remove the queue. Now you could logout from all the VMs.

####Organize your data
1. After you’ve got all the values, you can use tools like excel to create a spreadsheet and organize your data like
![spreadsheet](http://i.imgur.com/mFDx3TR.png)
and you should fill all the blue columns with your recorded data from the above steps. 
2. For the remaining columns, you could use the follow formulas:
	+ Average packet size(bytes) = bytes received / total packets

	+ mu(packets departure rates) = 10^6 / (Average packets size * 8)
	+ rho() = Average packet rate / mu
	+ analytical queue length = (1-rho/2)*rho/(1-rho)-rho

Ofcourse you need to change them to the specific cells. For example, to calculate the value in J46, which is the analytical queue length when you set the lambda 50 and the seed 0.4. After you get the value of rho, you can set the formula like `=(1-H46/2)*H46/(1-H46)` where `H46` corresponds to the cell of rho with the same parameters.

####Visualize your results
1. Open the file time.csv on your local machine with the tool like excel, you’ll see the interarrival time column. Insert a blank row “row1”, and then set the calculating interval 0.0005, you should have two columns like

	![Imgur](http://i.imgur.com/7nLYjLI.png)

	The total number of intervals should be large enough so that the union of those intervals could cover almost all the interarrval times that you’ve collected.

	Then use the formula `countif`, `concatenate` and `sum` in excel to count the interarrival time. One count in an interval, say, 0.03-0.035 in row 8 should have formula like

		=COUNTIF($A$2:$A$6387,CONCATENATE("<=",$G8))-SUM($H$2:$H7)
		
	where A2 and A6378 is the start and end row of your collected data, G8 is 0.035, H2 and H7 is the start and end cell which contain the number you’ve already used in the former rows. 

	Finally, save this file and rename it as time.xlsx.


2. You can use tools like matlab to visualize your results. After you collect all the data and put them in a spreadsheet, run the matlab script plot.m.

	Modify the script ditg.m to make sure that you use the data you’ve just collected. For example, this part

		rho = xlsread('ditg.xlsx', 'H38:H62'); %rho=lambda/mu
		alen = xlsread('ditg.xlsx', 'J38:J62'); %analytical queue length
		elen = xlsread('ditg.xlsx', 'L38:L62'); %experiment queue length

	What should be modified is the file name(here I have 'ditg.xlsx') and the position that your data locate in spreadsheet(like 'H38:H62').

	Similarly, to verify the distribution of interarrival time, the variable t and count should also be modified with your actual file name and the exactly location of the data.

		t = xlsread('a75_1.xlsx', 'G2:G201'); %interarrival time

		count = xlsread('a75_1.xlsx', 'H2:H201'); %number of interarrival time

	Run the script and you'll got two plots. One is the relationship between the average queue length and rho, and another one is the counts of interarrival times for all the packets.


###ns2
####Preparations
1.	Log in to a node 
You can do this experiment on any node in the same topology in the last experiment.
Run

		ssh username@hostname -p port –i ~/.ssh/id_rsa

	on your local machine.
Still, you may have different path where you private key locates.
2. On that VM, run

		sudo apt-get update
		sudo apt-get install ns2
	
3. On local host, run

		scp -P <port> path1/md1.tcl username@hostname:path2

	to save the script to the remote node. Here path1 is where the script md1.tcl locate in your local machine and path2 is where you want to store that script in the VM.
	
####Experiment Starts
1. Run script with 

		ns m11.tcl 1

	which will produce two files: out.tr and qm.out and 1 is the seed.

2. Run
		
		awk -F "[: ]" '{print $1,","$6}' qm.out >  newname.csv
	to extract the first and the sixth column of the file. Then on your local machine run
	
		scp -P <port> username@hostname:path/newname.csv ./

	to copy that file to your local machine. Open that file, find the value in the last row of the second column. This is the total number of packets that arrive at the queue. Divided this value by time(59.9sec), which can also be found from the value in the last row of the first column then you'll get the value of 𝜆. 

3. Run

		cat qm.out | awk '{sum += $5} END {if (NR>0) print sum/NR}' 
	to find the average queue length.
	
4.	Run 

		cat out.tr | awk '{if ($1 == "+") print $2","$6}' > pkt.csv

	to select the columns which represent the packet enter the queue and redirect the time and packet size columns to a new file so that you could check the distribution of the packet interarrival times as well as the packet size which should be a constant.
	
5. Then run

		scp -P <port> username@hostname:path2 /pkt.csv ./
	
	in your local machine to copy the out.tr file to your laptop.
	
####Repeat the experiments

Repeat the last step for four more times. The number 1 in command

		ns m11.tcl 1
	
should be modified to 2,3,4,5 so that you have five results with different seeds. Collect the data.

####Repeat with different parameters
Repeat the above two steps for four more times. Change the value of lambda in script md1.tcl to 50, 100, 150, 200, 230. And collect the data to fill all the black columns.

![Imgur](http://i.imgur.com/k9P7fN0.png)

The remaining columns is calculated from the data you’ve collected

+ Realized lambda=<arrivals(pkt) in 59.9sec>/59.9
+ mu=10^6/(8* Realized lambda)
+ rho = realized lambda/mu
+ average queue length = (1-rho/2)*rho/(1-rho)-rho

Finally, save the spreadsheet as a .xlsx file.

####Visualize your results
1.	Open the file pkt.csv with a tool like excel, you should notice that you have column with constant number 512. The first column is the time so you need to get the interarrival times by subtract the cells in first column with its preceding cell. For example, if you want to locate the interarrival time in column D, then the value D4 should be `=A4-A3`.

	Set the interval in another two columns so that you can count the number interarrival times in that interval. You’ll get two columns like
	
	![Imgur](http://i.imgur.com/UwCJavj.png)
	
	The total number of intervals should be large enough so that the union of those intervals could cover almost all the interarrval times that you’ve collected.

	Then use the formula `countif`, `concatenate` and `sum` in excel to count the interarrival time. One count in an interval, say, 0.03-0.035 in row 8 should have formula like

		=COUNTIF($D$2:$D$12160,CONCATENATE("<=",$G8))-SUM($H$2:$H7)

	where D2 and D12160 is the start and end cells of your data(interarrival times) in column D, G8 is 0.035, H2 and H7 is the start and end cell which contain the count number you’ve already used in the former rows.

	Finally, save this file and rename it as a .xlsx file.

2. Modify the matlab code ns2.m to make sure that you use the data you’ve just collected to visualize the results. What should be modified is the file name and the position that your data locate in spreadsheet.

![Imgur](http://i.imgur.com/ElK6rvE.png)

Similarly, to verify the distribution of interarrival time, this part should also be modified with your actual file name and the exactly location of the data.

![Imgur](http://i.imgur.com/H5eN7gn.png)

Run the script and you’ll got two plots—the average queue length and the interarrival time distribution.

After you've done all the steps, you can logout from the VM.

#My results
From the data I could see that the packet sizes are indeed the same. To verify the interarrival time distribution, in D-ITG experiment I use the data from the tcpdump output and extract a specific column while in ns2 I extract columns from the trace file and by some processes I use matlab to draw the distribution of interarrival times. The following two plots are the interarrival time distribution when I set lambda = 150. (I keep the y-axis the counts rather than normalized them).

![Imgur](http://i.imgur.com/8ci9xux.png)

![Imgur](http://i.imgur.com/SuSngj2.png)

These two figures tell me that the interarrival times do have an exponential distribution in both experiments. And by plotting similar figures in the repeating experiments with different parameters I could get exponential distributions too.

I also plot the average queue length over , which is also the relationship I want to verify.

![Imgur](http://i.imgur.com/KjoZu18.png)

![Imgur](http://i.imgur.com/bzhfJp0.png)

From those figures I could see that the realized queue lengths are very near the analytical values.

However, I also notice that in the left figure when  gets close to 1, the the realized values are quite greater than the analytical values. 
Also from the standard deviation and SE of the realized queue length, I could see that when  gets larger, the standard deviation and SE also gets larger. When , the standard deviation is 5 and SE is 2.3, which are greatly larger than that of small . Therefore, for , I did 20 more times with the same parameters. 
In 25 times repeated experiments, the mean average queue length was 14.6597691.46 (95%confidence interval). 

***
The software version :

Matlab R2015a(8.5.0)

Microsoft Excel 15.17


