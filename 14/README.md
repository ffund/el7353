### NYU Poly  EL7353 Network Modeling and Analysis  Lab 3,4,5
## Statistical Multiplexing Compared with Time- and Frequency-Division Multiplexing

### 1. Background
As we have learned in class, the M/M/1 queueing system consists of a single queueing station with a single server. Customers arrive according to a Poisson process with rate λ, and the probability distribution of the service time is exponential with mean 1/μ sec. 

Now, assume that m statistically identical and independent Poisson packet streams each with an arrival rate of λ/m packets/sec are transmitted over a communication line. The packet lengths for all streams are independent and exponentially distributed. The average transmission time is 1/μ. 

* For statistical multiplexing, the streams are merged into a single Poisson stream with rate λ, and then we get the average delay per packet. 
>
>T = 1 / ( μ - λ )
>
* For time- and frequency-division multiplexing, the transmission capacity is divided into m equal portions, one per packet stream, and each portion behaves like an M/M/1 queue with arrival rate λ/m and average service rate μ/m. Then we get the average delay per packet, which is m times larger than for statistical multiplexing. 
>
>T = m / ( μ - λ )
>

The picture below shows the three systems which would be implemented in this experiment. 

![enter image description here](https://lh3.googleusercontent.com/-NlHEq0U_Onk/VvDEz0Xc3UI/AAAAAAAAAAg/43ZJh_oRDLgpiHwIYrRyjHP94X1VzEKHg/s0/2.jpg "2.jpg")

If you have more questions about the background and the design of my experiment, please refer to my PDF report. 

### 2. Results
According to the theoretical analysis, we can get the basic theory results as shown in the following table.  

 Theory      |    λ  |  μ      |  ρ     |  N      |  T     |
 ----------: | :----:| :-----: |  :---: | :-----: | :----: |
 System1 m=1 |   225 |  244.14 |  0.92  |  11.76  |  0.052 | 
 System2 m=2 | 112.5 |  122.07 |  0.92  |  11.76  |  0.104 |
 System3 m=3 |    75 |  81.38  |  0.92  |  11.76  |  0.157 |

In this experiment, we verify the linearity relationship between T and m, as the picture shows below. 

![enter image description here](https://lh3.googleusercontent.com/-z-seLBTn21k/VvC-XdEnSvI/AAAAAAAAAAM/r2-D7yLKwPgHa48dMKbKYIttIhXVkfNOw/s0/1.jpg "1.jpg")

This is just the scenery of the statistical multiplexing compared with time- and frequency-division multiplexing.  The average queue length is the same both for statistical multiplexing and time- or frequency-division multiplexing. But the time delay of time- or frequency-division multiplexing is m times larger as the statistical multiplexing’s. 

### 3. Run my experiment
(1) Set up topology and install required software

Create a new slice on the "GENI" portal and a "client-router-server" topology in the slice, then choose an proper aggregate. Remember to install **D-ITG** on your client and server nodes, and designate one note to run your simulations. The code is as follow. 

    ssh UserName@HostName -p PortNumber -i /Path/Key
    sudo apt-get update
    sudo apt-get install d-itg
    sudo apt-get install ns2


(2) Testbed experiment

Set up the queue on your router to pass traffic at a rate of 1 Mbps, with the maximum size of the queue of 200 MB. 

    sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600

To watch the queue size, you need the bash script called **queuemonitor.sh**, you can upload it from your host to your router, and then make it executable. 

    scp -i /Path/Key -P PortNumber ~/Path/queuemonitor.sh UserName@HostName:~/Path/
    chmod a+x queuemonitor.sh

On your server, run

    ITGRecv

One your client, run the following code with certain "MEAN-ARRIVAL-RATE" and "MEAN-PACKET-SIZE", for example 225 and 512 at the first five experiments. 

    ITGSend -l SENDER-LOG -x RECEIVER-LOG -E MEAN-ARRIVAL-RATE -e MEAN-PACKET-SIZE  -t 85000 -T UDP

After about five seconds, start the **queuemonitor** script on the router node. 

    ./queuemonitor.sh eth2 60 0.1 | tee router.txt

At the same time, run **ping** on another terminal which has logged into the client. 

    ping server

Press **Ctrl+C** to terminate the ping after the **queuemonitor** has ended. Record the average round trip time measured by the **ping** as the time delay. 
Run the following code on the router to find the average queue size and record it. 

    cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }'

After the **ITGSend** ends, press **Ctrl+C** to terminate the **ITGRecv** on the server. Use the following code to decode the log file to find and record the actual packet arrival rate, the total number of packets received and the total received bytes. 

    ITGDec RECEIVER-LOG

Then you will have all the data needed in one experiment. To be more accurate, you should repeat all these steps five times to get five groups of data. 
Then you should change the "MEAN-ARRIVAL-RATE" and "MEAN-PACKET-SIZE" to 112.5 and 1024, and repeat the experiment five times. And then change them into 75 and 1536, and repeat the experiment five times. Now, you have finished the testbed experiment and you should go to calculate the parameters you need from the data you measured, which is very easy. 

(3) NS2 simulation

First you should set the certain value of "lambda" and "mu" in the **mm1.tcl** on your own host, for example 225 and 244.14 at the first five experiments. Then upload the **mm1.tcl** to your designated node on GENI and run an ns2 simulation after login. 

    scp -i /Path/Key -P PortNumber ~/Path/mm1.tcl UserName@HostName:~/Path/
    ns mm1.tcl RepValue

Remember to change the "RepValue" when you do the five experiments, for example from 1 to 5. Then use the following code to compute the average value of queue size in packets. 

    cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }'

Download the **qm.out** document to your host and open it to see and record the total packets arrival, total packets departure and total bytes departure. 

    scp -i /Path/Key -P PortNumber UserName@HostName:~/Path/qm.out ~/Path/

Upload the **delay.awk** document to your client, and run the following code to calculate the delay. 

     scp -i /Path/Key -P PortNumber ~/Path/delay.awk UserName@HostName:~/Path/
     awk -f delay.awk < out.tr | tee 1.txt
     cat 1.txt | awk  '{ sum += $2 } END { if (NR > 0) print sum / NR }' 

Then you have got all the data which should be measured in one experiment. Then you should only change the "RepValue" to repeat all steps five times. 
Then you should change the "lambda" and "mu" in the **mm1.tcl** document into 112.5 and 122.07, and repeat the whole five-time experiment. And then change them into 75 and 81.38, and repeat five times. Now, you get all the date and then the experiment finishes.