# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* In this experiments, we validate theoretical results related to the M/D/1 queue on GENI and ns2

### Back ground###
In this experiment, I am going to measure the average queue length in the following scenario:

![123123123.jpg](http://i.imgur.com/msp7DpL.jpg)

We know that the mean number of packets in this system is given by:

>  (1-ρ/2)*(ρ/(1-ρ))

Where 

>  ρ=λ/μ

λ and μ are the rate at which packets arrive at the queue (in packets/second) and the rate at which packets may be served by the queue (in packets/second). Generally, the distribution of μ comes from variations in the size of packets arriving at the queue, which has a constant service rate in bits/second - μ is computed by dividing the queue service rate in bits/second by the average packet size in bits.

From the mean number of packets in the system, we can compute the mean number of packets in the queue by substracting the mean number of packets in service:

>  length=(ρ(1-0.5ρ))/(1-ρ)-ρ

This mean queue length is the quantity I will be measuring in this lab assignment.

### How do I get set up? ###

* Set up topology and install required software

To start, create a new slice on the GENI portal, using the template "lab3-NETID" (with your own net ID) for the slice name. Create a client-router-server topology like the following picture, using the same guidelines for choosing an aggregate.

![13333.png](http://i.imgur.com/qkk3IxE.png)

Once your resources come up, verify that traffic between client and server goes via the router, and find out the properties (latency, capacity) of the links.

* Preparation of experiment

To generate traffic with exponentially distributed interarrival times and determined packet sizes, I will use the D-ITG traffic generator. 

You will need to install D-ITG on your client and server nodes. Log in to each and run:

`sudo apt-get update # refresh local information about software repositories`

`sudo apt-get install d-itg # install D-ITG software package1`

For ns2, you will need to install ns2, using following codes:

`sudo apt-get update # refresh local information about software repositories`

`sudo apt-get install ns2 # install ns2 network simulator`


* How to run tests

  1. Geni

+ Record the requested λ and μ in your experiment log.

+Set the router

>`sudo tc qdisc replace dev eth1 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600`

+ Start ITGRecv on the server node.

>`ITGRecv`

+ Start ITGSend with the appropriate arguments and with a long experiment duration on the client node.

>`ITGSend -a server -l sender.log -x receiver.log -E 200 -c 512 -T UDP`

+ After about five seconds, start the queue monitor script on the router node. Set it to run for a duration of about 40 seconds. 

>`./queuemonitor.sh eth1 40 0.1 | tee router.txt`

+ When the queue monitor script ends, find the average queue size and record it in your experiment log.

>`cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }'`

+ When ITGSend ends, use ITGDec on the server node to decode the receiver log. Find the actual packet arrival rate (λ) and the actual average packet size (μ is equal to the queue rate, divided by the average packet size in bytes.) Add your realized values for λ and μ to your experiment log. (Use src to download the decoded log file and use Excel to calculate)

+ Given the realized values of λ and μ in your experiment, calculate the realized value of ρ and then calculate the theoretical average queue size for this value of ρ.

  1. ns2

+ Run the ns2 script with a random seed.

>`ns md1.tcl 1`

+ You can use the following command to get the average queue length.

>`cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }'`

+ The information is saved in qm.out, you can save it as “qm***.csv”and open it in Excel to get more advanced information (the following picture is a example)

![1.png](http://i.imgur.com/BJXiHqP.png)

![2.png](http://i.imgur.com/GrvnKnf.png)

![3.png](http://i.imgur.com/NzLAcJK.png)

### Result ###

![11.png](http://i.imgur.com/JklWUyj.png)

![22.png](http://i.imgur.com/P3HXr1q.png)

![33.png](http://i.imgur.com/CWZEIEn.png)

![44.png](http://i.imgur.com/88nZ8Ap.png)

![55.png](http://i.imgur.com/bYD3q1y.png)