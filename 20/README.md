# Background
In a M/G/1 queue, the average queue lenghth according to analytical result would be E(N)=ρ/(1-ρ)*[1-ρ/2*(1-μ*μ*σ*σ)]
. We are interested in the way that average queue length changes as the STD of serviece time change from 0 to 0.0012.

# Result
![fig1.jpg](http://i.imgur.com/RYfVSV4.jpg)


Analysis of result: As plotted above, our result data is close to the analytical. But the result is not close enough to the analytical one. Because our STD value is rather small, there is little difference (about 0.1) between analytical expectation of nearby points. 

The rational intuition is to increase the difference between different STD values and achieve more difference between queue length of nearby points. However, it is hard to achieve high STD in normal distribution for packet size. For the NS2 experiment, as we increase STD, the probability of packet size being smaller than zero gets higher. In fact, when STD is 0.0012 we would get errors caused by packet size being equal or smaller than 0 for certain seeds. How about increasing the mean packet size and the STD? But then our service rate would also increase to keep ρ the same. Thus there is still little change in STD. 

Then what about using a different distribution? The uniform distribution has been tested, giving similar result. And as the variance of the uniform distributed variable is (a-b)*(a-b)/6 where a and b are the minimum and maximum possible values. This gives us no more variance than normal distribution. 
The other extreme case where packet sizes could be one of 2 values has also been tested. We let packet size take either 1 or 1023 in NS2. This gives us large STD but still unsatisfactory result. It may be because that the packets with 1 is so close to zero that this cases is like having half arrival rate with mean packet size of 1023. So this kind of test does not give us the desired result when STD is rather high. 

The other possible cause for this imperfect fit with analytical result is that the experiment time is not long enough. In fact, when we increase the experiment time to 500 seconds, we still get the similar results as our 120 seconds long simulation. Meanwhile, the running time increases significantly. What about  adding more replications of tests? We have increased our replication from 5 to 10. We gained some but not significant improvement. It is possible that our result would fit the analytical value better if we run hundreds of replications, but that takes much more time and is bad for the replication of this experiment.  
Has there been any mistakes? I have checked that there is no drop of packet in our simulation. And all the parameters have been checked several times. No mistake has been found. And the analysis of trace files shows that the statistics of the packet size and arrival rate meet our requirement.

Overall, the choice of this topic brings about some complications that leads to a not so perfect result. But we can still verify that average queue length would increase as the STD of service time increases given that other parameters remain constant. 

# Run my experiment

Instruction of running the NS2 script:

Run `ns mg1final.tcl 1 225.0 0.0` in the terminal. The first number after the file name represents the seed number. The second number represents value of λ. The third number represents the standard deviation of packet length, which is linearly related to the standard deviation of service time. Change the third value from 0 to 37.5, 75, 112.5,150 and run the script 5 times.

For each value of standard deviation, run 5 replications. Each replication should use a different seed number.
After each replication, run `cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }` in the terminal and get the average queue length. Then in another terminal, use scp to copy the file named out.tr to your computer and rename it to mg1.csv. Drag the file mg1.csv to MATLAB’s command window. A variable import window would pop up, make sure you have selected all the variables and click import selections. Run the MATLAB file lab3proc.m and you will get the value for realized λ,μ,ρ,σ.
The content of lab3proc.m is also attached below, you can copy the command and run them in MATLAB.
(Note: for large STDs, some seeds may cause error. You can try other seeds.)

Instruction of running testbed experiment:

1.	Set up a client-router-server topology in GENI.
2.	Install ITG in server and client.
3.	Type `sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600` in the router.
4.	Start ITGRecv on the server node
5.	Start ITGSend on the client node. We can use normal distributed packet size with the option –n mean std. (`ITGSend -a server -l sender.log -x receiver.log -E 225 -n 512 0  -t 85000  -T UDP`)
6.	After about 5 seconds, start the queue monitor script used in the [M/M/1 experiment](http://witestlab.poly.edu/~ffund/el7353/2-testbed-mm1.html) (`./queuemonitor.sh eth2 70 0.1 | tee router.txt` ). Set it to run for about 70 seconds.
7.	When the queue monitor script ends, the average queue size can be found and recorded in the log
8.	When ITGSend ends, use ITGDec on the server to decode the receiver log. Run `ITGDec  receiver.log -P >sizes.txt`, and use scp to copy size.txt to your computer and rename it to sizes.csv. Then drag the file to command window of MATLAB. Run lab3proc2.m. You can see the computed value of all the parameters and record them in the experiment log.
9.	Change the std of packet size from 0, 37.5, 75, 112.5 to 150. Execute 5 or 10 or more replications for each std value.