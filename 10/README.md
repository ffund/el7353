# README #

This README would id used for reproducing the M/D/1 queue.

## **Background**##

In this experiment, I will validate the theoretical results related to the M/D/1 queue with a series of experiments on the GENI testbed and the ns2 simulator.
In this experiment, we're going to measure the average queue length in the following scenario:

![Screen Shot 2016-03-20 at 16.24.16.png](http://i.imgur.com/ZqzmUPg.png)

For a M/D/1 system, the inter-arrival  rate λ  should be set to exponential and service rate μ should be set to deterministic.
From the lecture, we know in M/D/1 queue, the mean number of packets in this system is:

![Screen Shot 2016-03-22 at 01.45.27.png](http://i.imgur.com/HnLWBoU.png)

where

![Screen Shot 2016-03-22 at 01.45.31.png](http://i.imgur.com/Gsao6hb.png)

and λ and μ are still the rate at which packets arrive at the queue (in packets/second) and the rate at which packets may be served by the queue (in packets/second). Generally, the distribution of μ comes from variations in the size of packets arriving at the queue, which has a constant service rate in bits/second - μ is computed by dividing the queue service rate in bits/second by the average packet size in bits.
From the mean number of packets in the system, we can compute the mean number of packets in the queue by substracting the mean number of packets in service:

![Screen Shot 2016-03-22 at 01.49.48.png](http://i.imgur.com/qwmoXJy.png)



## **Results** ##

This mean queue length is the quantity we will be measuring. You will create a plot similar to this one:
![Screen Shot 2016-03-22 at 01.51.38.png](http://i.imgur.com/xUOpfqf.png)

You will also measure how well your realization of the M/D/1 queue matches its expected properties, with a plot of "realized" ρ versus expected ρ, like this one:
![Screen Shot 2016-03-22 at 01.51.49.png](http://i.imgur.com/1d8vAXN.png)

## **Run my experiment** ##

To reproduce the results, you should follow the instructions below:

### 1.Set up topology and install required software ###
To start, create a new slice on the GENI portal, using. Create a client-router-server topology like the one in lab 1, using the same guidelines for choosing an aggregate.

Once your resources come up, verify that traffic between client and server goes via the router, and find out the properties (latency, capacity) of the links.

To generate traffic with exponentially distributed interarrival times and deterministic distributed packet sizes, we will use the D-ITG traffic generator. Make sure you have install both D-ITG and ns2.
### 2. Use D-ITG in the client and server to generate the M/D/1 queue ###
Try it now with the following command on the receiver (server) node:
> ITGRecv

Leave that running, and on the sender (client) nodes, run:
> ITGSend -a server -l sender.log -x receiver.log -E 200 -c 512 -T UDP -t 85000

This will send UDP traffic to the server with a packet interarrival time that is exponentially distributed with mean 200 packets/second, packet size that is deterministic distributed with mean 512 bytes, and will save the log files at the client node and server node with file names "sender.log" and "receiver.log" respectively.

Wait for the sender to finish (since we have not specified a duration, it will run for the default duration of 10 seconds) and then stop the receiver with Ctrl+C. Then, open the log on the receiver side with
> ITGDec receiver.log

and on the sender side with
> ITGDec sender.log

Here you can see some basic aggregate information about the generated traffic flow(s). 

In general, the basic usage of the sender will be as follows:
> ITGSend -l SENDER-LOG -x RECEIVER-LOG -E MEAN-ARRIVAL-RATE -e MEAN-PACKET-SIZE  -t EXPERIMENT-DURATION -T UDP

### 3. Deploy a queue ###
The traffic source and sink applications will run on the client and server nodes. On the router node, we will deploy a queue, using the Linux Traffic Control (tc) utility to manipulate queue settings. 

For this assignment, we will use a token bucket filter queue. This queue shapes traffic using tokens, which arrive at a steady rate into a token bucket (which has a maximum capacity equal to the "burst size" of the queue). Meanwhile, traffic arrives at the token bucket from a separate queue. To leave the queue (and be transmitted), a packet must consume a number of packets roughly equal to its size in bytes. If there is not a sufficient number of tokens available when a packet arrives at the head of the queue, it must wait for more tokens to be generated. The queue, like the token bucket, has a finite length; if a packet arriving at the queue finds it full, the packet is dropped.

Try setting the queue to serve traffic at a rate of 1Mbps by running the following command on the router:
> sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mau 1600

Here, I have specified the interface on which to queue outgoing traffic (use the interface that is directly connected to the server node - in my case, eth2), the rate at which the queue should operate (1 Mbps), and the maximum size of the queue (200 MB, i.e. "very large"). The burst setting (15 kB) was set by trial and error - I used the minimum burst setting that avoids packet loss. The peakrate, which refers to the rate of a second "peak smoothing" bucket, is set to a value only slightly higher than the queue rate, and the size of the second bucket is set with the mtu parameter.

The tc tool includes a command to show the current state of a queue. Try running it, specifying as the last interface the name of the network interface you want to monitor (e.g. eth2 in this example):
> tc -p -s -d qdisc show dev eth2

The output of this command may look something like this:
> qdisc tbf 8004: root refcnt 2 rate 1000Kbit burst 32Kb/1 mpu 0b peakrate 1010Kbit mtu 1599b/1 mpu 0b lat 1677.5s linklayer ethernet 
 Sent 267270918 bytes 496620 pkt (dropped 1081, overlimits 465790 requeues 0) 
 backlog 0b 0p requeues 0 

Use the script **queuemonitor.sh** we used before, and then make it executable (e.g. chmod a+x queuemonitor.sh).

To use this script, run
> ./queuemonitor.sh INTERFACE DURATION INTERVAL

where INTERFACE is the name of the network interface on which the queue is running, DURATION is the total time for which to monitor the queue, and INTERVAL specifies how long to sleep in between measurements.

Try running it now, on the router node. You may find it useful to simultaneously watch the output on stdout and redirect it to a file at the same time with tee, e.g.
> ./queuemonitor.sh eth2 40 0.1 | tee router.txt

After an experiment is over, you can process this file with your data analysis tool of choice. For your convenience, if you have redirected the output to router.txt, you can retrieve the average queue size with
> cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }'

This prints the contents of the file to the terminal, then uses sed to replace instances of "p" with a blank space " " (since the value we are interested in is in the form "2p"). Finally, it uses awk to find the mean value of the 37th column.

To remove a traffic shaping queue, you can run
> sudo tc qdisc del dev eth2 root

### 4. Developing the M/M/1 script in ns2 ###
Use the ns2 script called d1.tcl I give in the repository. 
> ns md1.tcl

This script produces two output files: "out.tr" and "qm.out". The "out.tr" file is known as a "trace file," 

The "qm.out" file is a trace file that specifically monitors the queue. Its columns are: Time, From node, To node, Queue size (B), Queue size (packets), Arrivals (packets), Departures (packets), Drops (packets), Arrivals (bytes), Departures (bytes), Drops (bytes).

We can use awk to compute the average value of the fifth column (queue size in packets) without having to transfer the trace file to another computer to use another data analysis program. Simply run
> cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }' 

### 5. Repeat both testbed and ns2 experiment several times ###
you will run ITGSend with a mean packet size of 512 B, but varying packet rate. Specially, you should run experiments for the following values of λ: 225, 200, 175, 150, 125. For each value of λ you should run 5 experiments.

Given that you have already set up the queue as described above, to run a single experiment, you will:

* Record the requested λ and μ in your experiment log.
* Start ITGRecv on the server node.
* Start ITGSend with the appropriate arguments and with a long experiment duration (e.g. 85 seconds) on the client node.
* After about five seconds (but within ten seconds or so), start the queue monitor script on the router node. Set it to run for a duration of about 60 seconds. (We want to make sure that the queue monitor script only runs while traffic is being sent, to avoid distorting the average queue size.)
* When the queue monitor script ends, find the average queue size and record it in your experiment log.
* When ITGSend ends, use ITGDec on the server node to decode the receiver log. Find the actual packet arrival rate (λ) and the actual average packet size (μ is equal to the queue rate, divided by the average packet size in bytes.) Add your realized values for λ and μ to your experiment log.
* Given the realized values of λ and μ in your experiment, calculate the realized value of ρ and then calculate the theoretical average queue size for this value of ρ.

Then we'll run the simulation version. Use "md1.tcl" script appropriately,

* Run experiments for the following values of λ: 225, 200, 175, 150, 125.
* For each value of λ you should run 5 experiments (with five different seeds.)
* For each experiment, you should record: the value of "rep," requested values for μ, λ, and ρ, realized values for μ, λ, and ρ, and theoretical and measured average queue length.