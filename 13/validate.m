close all
%validate
figure
x_ref = linspace(0,0.1,100);
lambda = 250;
t_ref = lambda*exp(-lambda*x_ref);
plot(x_ref, t_ref, 'g')
hold on
data = load('/Users/mengleizhang/Documents/tcpdump.txt');
timeNS2 = load('/Users/mengleizhang/Documents/time.txt');
sizeNS2 = load('/Users/mengleizhang/Documents/size.txt');

intervalNS2 = timeNS2(2:end)-timeNS2(1:end-1);
interval = data(:,1);
size = data(1:end-1,2);
[h1,l1] = hist(interval,30);
[h2,l2] = hist(intervalNS2,30);
delta1 = l1-[0,l1(1:end-1)];
delta2 = l2-[0,l2(1:end-1)];

plot(l1,h1./delta1./sum(h1),'o:r',l2,h2./delta2./sum(h2),':*b');
xlabel('Arriving Interval');
ylabel('Probability density function');
legend('Analytical','Testbed','Simulation');

figure
x_ref = 512;
size_ref = 1;
stem(x_ref, size_ref, 'g')
hold on
[h3,l3] = hist(size,30);
[h4,l4] = hist(sizeNS2,30);
delta3 = l3-[0,l3(1:end-1)];
delta4 = l4-[0,l4(1:end-1)];

plot(l3,h3./delta3./sum(h3),'o:r',l4,h4./delta4./sum(h4),':*b');
xlabel('Packet size');
ylabel('Probability density function');
legend('Analytical','Testbed','Simulation');