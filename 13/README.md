# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Background ##

In networking, the queueing delay is the time of a packet waiting in the queue before it get served. This term is often used in routers. When packets arrived at a router, the packets need to be processed before sending to the next hop. Since router can process one packet at a time, if the packets are arriving faster than the router can process, then packets are buffered in a queue before getting served by the router. Therefore, the queueing delay is related to the arrival rate and serve rate, denoted as **lambda** and **u**, respectively. According to the lecture notes, the average queueing delay is given by the following euqation,


```
E(T) = (1-p/2)/(u(1-p))

```

where **p** is the utilization, which equals **lambda** decided by **u**. In this experiment, I will use connect client and server with a router and evaluate the queueing delay by measure the average waiting time of each packet in the router buffer.

## Run My Experiment ##
### Testbed Experiment setup ###

After logging in to client and server, run the following commands to install D-ITG traffic generator,


```
sudo apt-get update # refresh local information about software repositories
sudo apt-get install d-itg # install D-ITG software package
```


At router, setting the queue to serve traffic at a rate of 1Mbps by running the following command,


```
sudo tc qdisc replace dev eth1 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600

```

Where the **dev** should be the interface connect to the server, in my case, it is eth1. Run the following command at server to start a ITG-server.


```
ITGRecv
```


Before sending UDP packet, ping the server at the client terminal to estimate the round trip time (RTT) without queueing delay, denote as T1. T1 is a reference of roughly RTT without any congestion.


```
ping SERVER_IP_ADDRESS

```

in my case, **SERVER_IP_ADDRESS** is 10.10.2.2. After this, let the client send UDP packets to the server, and ping the server at the client terminal right after client start sending packets. At the client, I fix the packet size as 512 Bytes and keep sending for 60 seconds. The variable is sending rate: 125, 150, 175, 200, 225 packets/second. the command is given below,


```
ITGSend -a server -l sender.log -x receiver.log -E SENDING_RATE -c 512 -t 60000 -T UDP

```

 When the packet transmitting stops, terminate the ping process to obtain the average RTT, denote as T2. The queueing delay equals T2. When the process completed, run the following to open log file at the server,


```
ITGDec receiver.log

```

From the log file, record the realized **sending rate**. The **average packet size** can be computed by dividing **Bytes Received** by **Total packets**, Then the **service rate** can be obtained by dividing service rate 1Mbps by **average packet size**.

I run 3 replications for each sending rate specified above, therefore the total experiment number is 15. For any one of the sending rate, I collects average arrival rate of 3 replications, average serve rate of 3 replications, max/min/average queueing delay of 3 replications. After collecting the traces, I input the results into Matlab scripts **lab3.m** to generate the plots.

### NS2 Simulation setup ###
First, install ns2 in any one of the nodes. I choose to install ns2 in the client use the following commands,

```
sudo apt-get update # refresh local information about software repositories
sudo apt-get install ns2 # install ns2 network simulator
```

I reused the script from lab2, but with some modifications. It is given in the repo, namely **mm1.tcl**. 

before running the script, use the following command to open **mm1.tcl** and modify the arrival rate,


```
nano mm1.tcl
```

I starts with sending rate 125 packets/second, and run the scripts with a random seed. The commands are given below,


```
ns mm1.tcl SEEDS

```

where each replication, SEEDS should be a different integer. After this, I use the following tcp command to copy the traces to local machine,


```
#!terminal

scp -i ~/.ssh/menglei -P 30523 menglei@pc2.instageni.wisc.edu:~/out.tr ~/Documents/

```

Go to the path of **out.tr** in your local machine, use the following two commands to obtain the time each packet arrive and leave the queue,



```
grep + out.tr | awk '{print $2}' >arriveTime.txt
grep -v 'r' out.tr | grep -v '+' |  awk '{print $2}' > leaveTime.txt
```


In order to compute average queue delay, I just need to use the leave time minus the arrival time. Note there would be more packets arrive than leave since when the simulation terminates, there still some packets in the queue. I just calculate the average delay for the packets already left. I also write a Matlab code **ns2Delay.m** to compute the queueing delay and arrival rate.

After collecting the traces, I replicate the simulation(125 packets/second sending rate) 3 time with different seeds. Then I change the sending rate to 150, 175, 200, and 225 packets/second, and also run 3 replications with different seeds. 

After obtain the delay and arrival rate for all the simulations, I can easily plug in the Matlab code **lab3.m** to generate plots for the ns2 simulation.

## Results ##

![Screen Shot 2016-03-22 at 11.12.53 AM.png](http://i.imgur.com/gwzjyOZ.png)

Fig. a shows the results of average queuing delay for both testbed and ns2 simulation, along with the analytical result. From the figure, when utilization is low, it is obviously that both testbed and simulation produce a smaller queueing delay compare with analytical solution. However, as the utilization goes up, the simulation results start to approaching the analytical solution, but the testbed ramp up even faster and returns larger queueing delay.