%lab2
data = [
        225, 244.14, 0.922, 218.158, 244.14, 0.0338, 0.0431, 0.0355;%---experiment 225
        200, 244.14, 0.819, 195.976, 244.14, 0.0052, 0.0083, 0.0074;%---experiment 200
        175, 244.14, 0.717, 170.928, 244.14, 0.0019, 0.0025, 0.0024;%---experiment 175
        150, 244.14, 0.614, 147.964, 244.14, 0.0013, 0.0016, 0.0014;%---experiment 150
        125, 244.14, 0.512, 122.588, 244.14, 0.0011, 0.0013, 0.0012;%---experiment 125
       ];

geni = zeros(5,10);
geni(:,1:3) = data(:,1:3);
geni(:,4) = (1-data(:,3)/2)./(244.14.*(1-data(:,3)));
geni(:,5:6) = data(:,4:5);
geni(:,7) = data(:,4)./data(:,5);
geni(:,8:10) = data(:,6:8);

fileID = fopen('geni.txt','w');
fprintf(fileID,'lambda(Req)\tMu(Req)\tp(Req)\tQdelay(Req))\tlambda(Real)\tMu(Real)\tp(Real)\tQdelay(min)\tQdelay(max)\tQdelay(mean)\n');
fprintf(fileID,'%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n',geni');
fclose(fileID);
fprintf('Done\n');


figure
label_ana = linspace(0,0.94,95);
queue_ana = (1-label_ana/2)./(244.14.*(1-label_ana));
plot(label_ana, queue_ana, 'g');
hold on

label_geni = geni(:,7);
time_geni = geni(:,10);
max_geni = geni(:,9);
min_geni = geni(:,8);

errorbar(label_geni, time_geni, time_geni-min_geni, max_geni-time_geni, 'r');

% data1 = load('/Users/mengleizhang/Documents/a.txt');
% data2 = load('/Users/mengleizhang/Documents/b.txt');
% interval = data2-data1(1:length(data2));
% mean_t = mean(interval);
% max_t = max(interval);
% min_t = min(interval);
% lambda = length(data2)/1000;

result = [
        225, 244.14, 0.922, 222.95,  244.14, 0.0213, 0.0291, 0.0226;%---experiment 225
        200, 244.14, 0.819, 197.067, 244.14, 0.0079, 0.0091, 0.0086;%---experiment 200
        175, 244.14, 0.717, 173.7,   244.14, 0.0051, 0.0055, 0.0052;%---experiment 175
        150, 244.14, 0.614, 150.45,  244.14, 0.0032, 0.0034, 0.0033;%---experiment 150
        125, 244.14, 0.512, 125.083, 244.14, 0.002,  0.0022, 0.0021;%---experiment 125
       ];
   
ns2 = zeros(5,10);
ns2(:,1:3) = result(:,1:3);
ns2(:,4) = (1-result(:,3)/2)./(244.14.*(1-result(:,3)));
ns2(:,5:6) = result(:,4:5);
ns2(:,7) = result(:,4)./result(:,5);
ns2(:,8:10) = result(:,6:8);

fileID = fopen('ns2.txt','w');
fprintf(fileID,'lambda(Req)\tMu(Req)\tp(Req)\tQdelay(Req))\tlambda(Real)\tMu(Real)\tp(Real)\tQdelay(min)\tQdelay(max)\tQdelay(mean)\n');
fprintf(fileID,'%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n',ns2');
fclose(fileID);
fprintf('Done\n');

label_ns2 = ns2(:,7);
time_ns2 = ns2(:,10);
max_ns2 = ns2(:,9);
min_ns2 = ns2(:,8);

errorbar(label_ns2, time_ns2, time_ns2-min_ns2, max_ns2-time_ns2, 'b');
xlabel('Ideal p');
ylabel('Average queue delay(s)');
legend('Analytical', 'Testbed', 'Simulation');


figure
label_ref = [0.922,0.819,0.717,0.614,0.512];
plot(label_ana(51:end), label_ana(51:end), 'g');
hold on

plot(label_ref, geni(:,7), ':bo');

plot(label_ref, ns2(:,7),  ':r^');
xlabel('Ideal p');
ylabel('measured p');
legend('Analytical', 'Testbed', 'Simulation');
