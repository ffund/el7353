# Data processing file 
# Execute this file after you are done collecting data
# 3 input arguments are given when this file is execute from terminal, Below is the order in which you are required to give arguments

#### Output folder name			like 		"output-2-2016-03-14-18-00"
#### Receiver log Folder name		like 		"receiverlog-2-2016-03-16-04-13"
#### csv file 				like		"data.csv"

output=$1
rxlog=$2
out=$3

mkdir out

tp=totalpkt.txt
qlen=qlen.txt
dp=droppkt.txt

cd "$rxlog"

for f in receiver_{25..150..5}_{1..5}.txt;
do
        fname=$f
        ITGDec $f | grep "Total packets" | sort | uniq > test.txt
        sed 's|Total packets            =         ||' <test.txt >> $tp
done

cp "$tp" ../out/
cd ..

cd "$output"

for f in router_{25..150..5}_{1..5}.txt;
do
        fname=$f
        tail -1 $f | awk '{print $30}' >> $dp
        sed -i 's/,/ /g' $dp	
done

cp "$dp" ../out/

for f in router_{25..150..5}_{1..5}.txt; do
       
	cat "$f" | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }' >>  $qlen
done

cp "$qlen" ../out/

cd ..

cd out/

#Check the length of output files generated
echo "checking lengths of output files generated"

cat "$qlen" | wc -l
cat "$tp" | wc -l
cat "$dp" | wc -l 

paste -d',' "$dp" "$tp" "$qlen"  > temp.txt
#awk  -F "," '{printf "%s,%s,%4.3f\n,%s", $1,$2,$1/$2,$3 }' temp.txt > $out
awk  -F "," '{print  $1","$2","$3","$1/$2 }' temp.txt > $out


