##Following are the commands that you will use with the R console to visualize data

R instructions
data <- read.csv('data.csv',header=FALSE,sep=",")

##Check data
head(data)

## Output should be somewhat like the below
    V1    V2      V3        V4
1  963 14826 40.7784 0.0649535
2 1223 14799 42.8585 0.0826407
...


##Add column names to all columns
colnames(data) <- c("drop","total","mqlen","Pb")

## Check whether your assigned column names are assigned
head(data)

## Output should be somewhat like the below
  drop total  mqlen    Pb  
1  963 14826 40.778 0.065 
2 1223 14799 42.858 0.083 
3 1098 14763 43.097 0.074 


###Calculate Mean queue length
data$mean <- rep(colMeans(matrix(data$mqlen, nrow=25), na.rm=TRUE), each=25)

##Check if the new column with name "mean" is added to the data frame

##Check data
head(data)

## Output should be somewhat like the below
  drop total  mqlen    Pb   mean
1  963 14826 40.778 0.065 42.555
2 1223 14799 42.858 0.083 42.555
3 1098 14763 43.097 0.074 42.555

##Calculate Mean Pb
data$meanpb <- rep(colMeans(matrix(data$Pb, nrow=25), na.rm=TRUE), each=25)


##Calculate unique values of Mean
umqlen <- unique(data$mean) 

umpb <- unique(data$meanpb)


##Calculate Analytical Pb
analytical <- eval(1/(umqlen+1))

##Plot Result and save in a png file
png('result.png')
plot(umqlen,analytical, type="o",col="red" , ylab="Blocking Probability", xlab="Mean Queue Size (packets)" , lwd=2, yaxt='n')
par(new= TRUE)
plot(umqlen,umpb, type="o",col="blue" , ylab="Blocking Probability", xlab="Mean Queue Size (packets)" , lwd=2)
legend("topright", legend = c("analytical ", "testbed "),col=c("red","blue"), text.width = strwidth("1,000,000"), lty = 1, xjust = 1, yjust = 1)
dev.off()


##Plot Result and save in a pdf file
pdf('result.pdf')
plot(umqlen,analytical, type="o",col="red" , ylab="Blocking Probability", xlab="Mean Queue Size (packets)" , lwd=2, yaxt='n')
par(new= TRUE)
plot(umqlen,umpb, type="o",col="blue" , ylab="Blocking Probability", xlab="Mean Queue Size (packets)" , lwd=2)
legend("topright", legend = c("analytical ", "testbed "),col=c("red","blue"), text.width = strwidth("1,000,000"), lty = 1, xjust = 1, yjust = 1)
dev.off()

`````
