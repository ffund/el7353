# M/M/1 queue module comparison #

This README would normally document how to run this M/M/1 experiment 

### compare the queue property by measuring queue delay ###

* Assume we have two queues: queue A has 2 lambda and 2 mu; the other one has two channels with 1 lambda and 1 mu seperately. By the formula E(t)=ro/(1-ro)/lambda, we know that the delay of A should be half that of B.
* I do this experiment by two techniques: ITG and NS2. In both methods I can generate traffic, record the time delay and find the relationship between two modules

### steps in ITG ###
* Use sudo tc to set up some restraint for the traffic in router
* Use client to ping the server to get the normal system round trip time shown as below

```

sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600

```

* Set the server to be a ITG receiver

```

ITGRecv
```

* Let the client send specific packets to server with lambda=225, mu=244 and time period=65s

```

ITGSend -a server -l sender.log -x receiver.log -E 225 -e 512 -t 65000 -T UDP
```

* While sending packets, open another client and ping the server, which can obtain the time with queue delay
* Calculate the difference between two times so we can get the delay of the queue A
* Add another channel and change lambda and mu to be half 
* Repeat the steps above and we can get the delay of queue B
* Plot the two delay in one figure and we can check if it meets our expectation

### steps in NS2 ###

* Write a mm1.tcl file to generate queue traffic
* Write a awk file to calculate the end-to-end delay
* run the awk file and redirect the output to another file

```

awk -f delay.awk < out.tr > hk.out
```

* Since the delay is in the second column of this output file, I can calculate the average delay by this

```

cat hk.out | awk  '{ sum += $2 } END { if (NR > 0) print sum / NR }' 
```
* Then I can modify the tcl file to change the lambda and mu, get another delay for queue B and compare the two delay

### result ###
* The collected data show that the delay of queue A is approximately half that of queue B with images shown below
* ![图片1.png](http://i.imgur.com/8CPjjUw.pngg)
* ![图片2.png](http://i.imgur.com/0PMxMcD.png)