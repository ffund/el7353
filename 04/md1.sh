#!/bin/bash
declare -A matrix
declare -A len

num_rows=10
num_columns=1
count=1;
#At the end of this nested for loop, you will get a 2D array whose columns
# are different values of lambda and rows are the different values of rep

for j in 125.0 150.0 175.0 200.0 225.0	#this simulates the values of lambda
do
	printf "Average Queue Length for lambda=$j and rep varying from 1-10"
	echo
	for i in 1 2 3 4 5 6 7 8 9 10	#this simulates the values of rep
	do
   		ns md1.tcl $i $j
		matrix[$i,$num_columns]="$(cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }')"
		printf ${matrix[$i,$num_columns]}" "
		grep "+" out.tr > out2.tr
		len[$count]=$(cat out2.tr | wc -l)
		count=count+1;
	done
	echo
	num_columns=num_columns+1;
done

