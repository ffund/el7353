# Experiment: Average Queue Length for M/D/1 Queue 



## Background ##

An M/D/1 queue has  a memoryless inter-arrival time distribution, deterministic service time, one server and an infinite buffer. From theoretical analysis, we obtain the following results for the mean queue length in the queue:

Our goal in this experiment is to verify this result through simulations and testbed experiment. Precisely, we are interested in finding out the dependency of average queue length on the utility, and verifying whether this does indeed correspond with the theoretical formulae. We are also interested in finding out the actual throughput of the system and comparing it with the theoretically computed value of throughput. This will give us insight into the dynamics of the system and how fluctuations might occur in real-world scenarios which we can’t predict in theory.

With regards to our experiment, we have one M/D/1 queue which has an exponential arrival rate and deterministic service rate. The queue has an infinite buffer (in practice, we set it to a very large value).


Our metric of concern is the Average Queue Length (NQ), and the actual realized arrival rate at the queue. We use a simple one client-one server topology (shown above) where the client and server are connected through a router. The system we are modeling has quite straightforward outcomes. We generate UDP traffic at the client and route it to there server via the router. The queue is implement as a token bucket filter queue at the router. Packets arriving in the queue can face either of two outcomes:

1. The queue is empty and the packet proceeds straight to the server where it is served in a deterministic amount of time.

2. The queue is non-empty, in which case the packet enters the buffer and waits till its turn to be served arrive. Needless to say, it is a first in first out queue.

**Experiment Parameters**

Queue Rate: Set at 1 Mbps

Arrival Rate (Exponential Distribution)

Packet Size (Deterministic) : 512 bytes

Duration: 60 secs


**Factors**

Arrival Rate (Takes on the values: 125, 150, 175, 200, 225)

Experiment
	We consider a simple client-server topology (like the one we used for the M/M/1 experiment) where the client and server are connected through a router and the queue is implemented at the router. The arrival rate is exponentially distributed and the service rate is deterministic (μ is equal to the queue rate, divided by the average packet size in bytes. We have constant queue rate and constant packet size, so μ is deterministic )
	We implement a simple experiment where we vary the arrival rate (from 125-225) and find out the corresponding average queue sizes. This enables us to plot average queue sizes versus utility and verify the theoretical results. Ten replications are done at each level to obtain better results. While doing the simulations and testbed experiment, we demand the system to operate at a certain arrival rate. However, this is not always exactly achieved and hence we also measure the actual throughput at each stage.


##Results##

![](https://bitbucket.org/AffanJaved/el7353-maj407/downloads/average_queue_length.jpg )
![](https://bitbucket.org/AffanJaved/el7353-maj407/downloads/average_realized_throughput.jpg)

Figure 1 shows the main result of the experiment, which is the variation of the average queue length with the throughput. Figure 2 shows how the actual throughput compares with the ideal values of throughput. We present a brief analysis for the reader as we wish to highlight certain things about the results obtained. 

To begin with, notice that the simulation and testbed results do verify the theoretical results. The average queue length increases non-linearly in all cases with an increase in throughput. The increase is more evident as the throughput reaches one. 
	
Another interesting observation here is that the experimental results follow the theoretical results more closely at smaller values of throughput. At larger values of throughput, there is more deviation from the theoretical results. This can be be due to two reasons. Firstly, the system becomes less and less stable as the throughput approaches one. This means that the system will take more time to reach steady state as compared to when the throughput is small. Hence, we need to run the same experiments for larger durations when throughput is large to obtain more accurate results. Consequently, if we keep the experiment duration constant it is inevitable that the results will be more accurate at low throughput and more deviant at higher throughputs. Secondly, the actual throughput achieved is different for each run of the experiment (as is evident from Figure 2). However, notice that we plot Average Queue Length against the Ideal Throughput. This means that while we are plotting the average queue length at a particular value of throughput, it might actually be the result for a slightly lesser or slightly larger value of throughput. As the gradient is much higher at higher throughputs, this will lead to more deviations than compared to the experiment runs at lower throughputs.
	
Lastly, we observe that the simulation and experiment results are actually lower than the theoretical results at lower values of throughput. One possible explanation for this is that in the practical system, lower throughput means the packets arriving see mostly empty queues in front of them and therefore spend less time waiting in the queue. This results in lower queue lengths than you would normally expect. Moreover, till a throughput of 0.8 we see see that the standard deviation is almost negligible, indicating that our statistics are quite reliable. 


## How to Run my Experiment ##

### ns2 Simulation ###
For the simulation implementation, we implement the ns2 script at the client node (this choice is arbitrary). We modify the script we used for the M/M/1 case. The following changes are made:

1. Packet size is set to a constant 512 bytes.

2. The upper limit of packet size remains at the default 1 kbyte value because we don’t need to extend this.

We now generate and use only one random number, which is used for seeding the exponential inter-arrival time distribution.

The script is run for different value of rep for each value of the arrival rate. We obtain the realized arrival rate from the out.tr file and the average queue size from the qm.out file.

**Instructions for ns2 Simulation**

* Transfer the md1.tcl and md1.sh files into your local directory.
* Make md1.sh executable by running chmod a+x md1.sh
* Run md1.sh by running ./md1.sh
*The average queue lengths for different values of lambda and rep will be displayed on your output screen. The rows represent different values of arrival rate (125, 150,175, 200, 225) and the columns represent different values of rep (1-10).
* Copy the results into your experiment log.   


### GENI Testbed Experiment ###
For the testbed implementation, we create a topology where one client is connected to one server via a router. The token bucket filter queue is implemented at the router using a queue rate of 1 Mbps. As the service rate is deterministic, we set the packet size to a constant 512 bytes. This results in a constant service rate of 250 pkt/s. 
	Using D-ITG we generate UDP traffic and route it from the client to the server for different arrival rates. This enables us to find the actual arrival rate and the mean number of packets in the queue. Data collected does indeed verify that the packet sizes are all constant at 512 bytes.

**Instructions for Testbed Experiment**

* Set up the required topology (one server and client connected via a router) on GENI.

* Install D-ITG on your server and client nodes. Log in to each and run:

          sudo apt-get update #refresh local information about software repositories
          sudo apt-get install d-itg # install D-ITG software package

* Set up the token bucket filter queue at the router:

   * Transfer the queuemonitor.sh script into your local directory on the router node.

   * Make the script executable by running: chmod a+x queuemonitor.sh

   * Set up the queue by running:

	      sudo tc qdisc replace dev eth1 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600
	Note: Use the interface directly connected to the server node- in my case, eth1

* Run the experiment in the following manner:

   * Start ITGRecv on the server node.

   * Start ITGSend on the client node as follows:

                     ITGSend -a server -l sender.log -x receiver.log -E 125 -c 512 -T UDP -t 80000
	Note: Syntax is : -E Mean_Arrival_Rate. You will use the following values of mean arrival rate during the experiment: 125, 150, 175, 200, 225

   * After about five seconds, start the queue monitor script on the router node. Run the following command:

	             ./queuemonitor.sh eth1 60 0.1 | tee router.txt
   * When the queue monitor script ends, run:

	             cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }’
	This gives the average queue length. Record this in your log.

   * Run “ITGDec receiver.log” on server node. Record the actual arrival rate in your log.

   * Repeat the experiment  ten times for one value of arrival rate. Then repeat the experiment for the following values of arrival rate: 125, 150, 175, 200, 225.
