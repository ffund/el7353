
set ns [new Simulator]
set rep [lindex $argv 0]
set rng1 [new RNG]

if { $argc != 2 } {
        puts "The md1.tcl script requires two numbers to be inputed."
        puts "For example, md1.tcl 2 225.0"
        puts "The first number is the value for rep (choose from 0-10). The second number is"
	puts "the value for lambda which should not exceed 250.0 and should be"
	puts "specified up to one decimal point."
	exit
    }

set rep2 [lindex $argv 1]


for {set i 1} {$i<$rep} {incr i} {
        $rng1 next-substream;
    }

set tf [open out.tr w]
$ns trace-all $tf

set lambda $rep2
set mu     250.0
#puts stdout "Running simulation for rep=$rep and lambda= $lambda"

set n1 [$ns node]
set n2 [$ns node]
# Since packet sizes will be rounded to an integer
# number of bytes, we should have large packets and
# to have small rounding errors, and so we take large bandwidth
set link [$ns simplex-link $n1 $n2 1000kb 0ms DropTail]
$ns queue-limit $n1 $n2 100000

# generate random interarrival times and packet sizes
set InterArrivalTime [new RandomVariable/Exponential]
$InterArrivalTime use-rng $rng1
$InterArrivalTime set avg_ [expr 1/$lambda]
set pktSize [expr 512]


set src [new Agent/UDP]
$ns attach-agent $n1 $src

# queue monitoring
set qmon [$ns monitor-queue $n1 $n2 [open qm.out w] 0.1]
$link queue-sample-timeout

proc finish {} {
    global ns tf
    $ns flush-trace 
    close $tf 
    exit 0 
} 

proc sendpacket {} {
    global ns src InterArrivalTime pktSize 
    set time [$ns now]
    $ns at [expr $time + [$InterArrivalTime value]] "sendpacket"
    set bytes [expr 512]
    $src send $bytes
}

set sink [new Agent/Null]
$ns attach-agent $n2 $sink
$ns connect $src $sink
$ns at 0.0001 "sendpacket"
$ns at 60.0 "finish"

$ns run
return;


