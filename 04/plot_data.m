clc;
clear all;
close all;

num=10; % number of replications at each level in simulation and testbed experiment
dur=60; % duration of experiment= 60 seconds

%% Enter data here into the following variables
%Simulation_Data

%Queue Length
lambda_125=[0.277628 0.275751 0.262114 0.274219 0.245126 0.272708 0.259715 0.269739 0.263571 0.261724];          %arrival rate=125
lambda_150=[0.487161 0.517879 0.507209 0.498475 0.42827 0.467062 0.459233 0.483211 0.463102 0.478309];          %arrival rate=150
lambda_175=[0.864974 0.941779 0.929488 0.922969 0.766306 0.860532 0.814997 0.920002 0.841069 0.886164];          %arrival rate=175
lambda_200=[1.69299 1.8993 1.98951 1.85236 1.54988 1.73973 1.5541 1.99845 1.78513 1.78018];          %arrival rate=200
lambda_225=[4.3649 5.0378 9.51784 4.8428 3.69164 4.35525 4.26303 5.56048 5.40098 5.53307];          %arrival rate=225

%Realized Arrival Rate (Enter values of arrival rate*duration into these
%variables)
actual_lambda_125=[7551 7474 7474 7450 7390 7457 7487 7520 7557 7434];     %ideal arrival rate=125
actual_lambda_150=[9078 9011 9122 8930 8808 8910 9026 9049 8993 9029];     %ideal arrival rate=150
actual_lambda_175=[10523 10485 10638 10509 10215 10386 10461 10597 10493 10565 ];     %ideal arrival rate=175
actual_lambda_200=[11999 11930 12159 11976 11709 11883 11929 12045 12072 12021];     %ideal arrival rate=200
actual_lambda_225=[13486 13377 13730 13469 13167 13344 13450 13537 13566 13547];     %ideal arrival rate=225

actual_lambda_125=actual_lambda_125./dur;
actual_lambda_150=actual_lambda_150./dur;
actual_lambda_175=actual_lambda_175./dur;
actual_lambda_200=actual_lambda_200./dur;
actual_lambda_225=actual_lambda_225./dur;


%Testbed_data

%Queue Length
lambdatest_125=[0.03 0.02 0.02 0.02 0.02 0.03 0.02 0.03 0.04 0.03];      %ideal arrival rate=125
lambdatest_150=[0.13 0.10 0.10 0.10 0.14 0.11 0.08 0.09 0.08 0.12];      %ideal arrival rate=150
lambdatest_175=[0.30 0.31 0.31 0.37 0.54 0.39 0.30 0.32 0.31 0.32];      %ideal arrival rate=175
lambdatest_200=[1.01 1.19 1.07 0.81 1.26 1.19 1.23 1.31 1.09 1.26];      %ideal arrival rate=200
lambdatest_225=[15.42 5.19 8.18 9.04 5.52 6.7 5.48 7.72 6.49 5.83 ];     %ideal arrival rate=225

%Realized Arrival Rate
actual_lambdatest_125=[121.76 120.26 123.36 121.42 121.55 120.22 124.50 119.19 123.78 122.69];     %ideal arrival rate=125
actual_lambdatest_150=[147.57 149.04 145.26 146.59 148.12 144.93 140.95 148.30 142.13 144.05];     %ideal arrival rate=150
actual_lambdatest_175=[168.62 169.32 168.96 170.02 172.29 171.42 169.46 169.10 169.84 168.42];     %ideal arrival rate=175
actual_lambdatest_200=[193.31 194.26 192.42 191.97 192.17 193.95 191.46 194.10 192.10 190.36];     %ideal arrival rate=200
actual_lambdatest_225=[219.87 214.93 215.55 217.72 216.95 213.80 215.88 216.88 214.91 211.19];     %ideal arrival rate=225


%% Don't change anything below this line. Any experimental data is to be entered into the variables above

%Theoretical Data
ideal_arrival_rate=[125:25:225];
ideal_service_rate=250*ones(1,5);

ideal_throughput=ideal_arrival_rate./ideal_service_rate;
avg_queue_size_theor=ideal_throughput+(ideal_throughput.^2)./(2.*(1-ideal_throughput));




%Average Queue Size

%a) Simulation Data
avg_queue_size=[mean(lambda_125),mean(lambda_150),mean(lambda_175),mean(lambda_200),mean(lambda_225)];
std_queue_size=[std(lambda_125),std(lambda_150),std(lambda_175),std(lambda_200),std(lambda_225)];

%b) Testbed Data
avg_queue_size_testbed=[mean(lambdatest_125),mean(lambdatest_150),mean(lambdatest_175),mean(lambdatest_200),mean(lambdatest_225)];
std_queue_size_testbed=[std(lambdatest_125),std(lambdatest_150),std(lambdatest_175),std(lambdatest_200),std(lambdatest_225)];


%Throughput
real_arrival_rate_sim=[mean(actual_lambda_125),mean(actual_lambda_150),mean(actual_lambda_175),mean(actual_lambda_200),mean(actual_lambda_225)];
real_arrival_rate_testbed=[mean(actual_lambdatest_125),mean(actual_lambdatest_150),mean(actual_lambdatest_175),mean(actual_lambdatest_200),mean(actual_lambdatest_225)];

simulation_throughput=real_arrival_rate_sim./ideal_service_rate;
testbed_throughput=real_arrival_rate_testbed./ideal_service_rate;


%% Plots

%Plot of Ideal Throughput vs Realized Throughput

figure;
plot(ideal_throughput, ideal_throughput,'-*r');
hold on;
plot(ideal_throughput, simulation_throughput,'-ob');
hold on;
plot(ideal_throughput, testbed_throughput,'-xk');

xlabel('Ideal Throughput (pkt/s)');
ylabel('Realized Throughput (pkt/s)');
title('Ideal Throughput vs Realized Throughput for M/D/1 Queue');
legend('Theoretical', 'Simulation','Testbed');

%Plot of Average Queue Size vs Ideal Throughput
figure;
plot(ideal_throughput, avg_queue_size_theor,'-*r');
hold on;
errorbar(ideal_throughput, avg_queue_size,std_queue_size,'-ob');
hold on;
errorbar(ideal_throughput, avg_queue_size_testbed,std_queue_size_testbed,'-xk');

xlabel('Ideal Throughput (pkt/s)');
ylabel('Average Queue Size (pkt)');
title('Average Queue Size vs Ideal Throughput for M/D/1 Queue');
legend('Theoretical', 'Simulation', 'Testbed');