# README #

### Table of Contents ###

1. Background: explaining a brief overview of what has been done in this project.
2. Results: Consist of key results of the project in addition to the possible explanations
3. Run my experiment: explaining step-by-step instructions to run the expriments and simulations and reproduce the results
------------------------------------------------------------------
### 1. Background ###
In this project, I am going to validate theoretical results related to the M/D/1 queue with a series of experiments on the GENI testbed and the ns2 simulator. I am also going to compare the results of M/D/1 queue with the results of M/M/1 queue we have from previous experiments. In theory, from P-K formula and after some simplification for M/D/1 queue we have: E[N_Q]=(rho^2)/(2*(1-rho)), where E[N_Q] is the mean value of queue length and rho is the utilization factor of the queue which is the fraction of packet arrival rate to the packet service rate. On the other hand, we also know from the queueing theory that for M/M/1 queue we have E[N_Q]=(rho^2)/(1-rho) with the same definitions of the parameters. Comparing these two formulas shows that in theory, the average queue occupancy for M//D/1 is a half of the average queue length of M/M/1 queue. In this experiment we will validiate these two formulas by testbed experiment and simulation first, and then we will compare them to investigate the aforementioned theoretical fact.


### 2. Results ###

Below figures illustrate the simulation and experimental results in addition to the analytical expectations for M/D/1 and M/M/1 queues. We can observe that for both of the queues and for low loads, the experimental/simulation results of average queue length are less than what we expect from theory. However, for high loads the experimental/simulation reults of queue length are larger than what we expect. Also, we can see that queue length of M/D/1 is approximately half of queue length of M/M/1 for the same values of ρ. 

![Alt text](https://bytebucket.org/el7353ss9242/el7353-ss9242/raw/6cd4f5effa87e84beaddc053bfa10bad1fa13727/Lab4/testbed-figure.png?token=2c60364eb15f5716ae922c895a34d1e9d37f7e0d)
![Alt text](https://bytebucket.org/el7353ss9242/el7353-ss9242/raw/6570fc126b742cd3e00520fbade07a601651658d/Lab4/simulation-figure.png?token=866bcc230ed195fdf97d785c5a0610af9fc06040)


### 3. Run my experiment ###

** 3.1. On GENI:**

3.1.1. Reserve 3 VMs on GENI. As before we call them client,router and server. Make client-router and router-server connections.

3.1.2. Log into the VMs and install D-ITG on client and server nodes:

		sudo apt-get update # refresh local information about software repositories
		sudo apt-get install d-itg # install D-ITG software package
		
3.1.3. Copy the queue monitoring script (queuemonitor.ch) to router node from your local computer using scp command. Make it executable:

		chmod a+x queuemonitor.sh 
		
3.1.4. Setup a token bucket filter on router node with appropriate parameters:
		
		sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600

3.1.5. Run traffic sink on server node:

		ITGRecv
		
3.1.6. Run traffic source on the client node ((mean)Pack-size=512, mean arrival-time= varying in {125,150,175,200,225}, time=85000 ms) 

M/M/1 case: 

				ITGSend -l SENDER-LOG -x RECEIVER-LOG -E MEAN-ARRIVAL-RATE -e MEAN-PACKET-SIZE  -t EXPERIMENT-DURATION -T UDP
				
Example: 

				ITGSend -a server -l sender.log -x receiver.log -E 200 -e 512 -t 85000 -T UDP
				
		
M/D/1 case:

				ITGSend -l SENDER-LOG -x RECEIVER-LOG -E MEAN-ARRIVAL-RATE -c PACKET-SIZE  -t EXPERIMENT-DURATION -T UDP
				
Example: 

	    		ITGSend -a server -l sender.log -x receiver.log -E 200 -c 512 -t 85000 -T UDP
		
3.1.7. After about 5 seconds, run the queue monitoring script on the router VM:

		./queuemonitor.sh eth2 40 0.1 | tee router.txt
		
3.1.8. After the the transmission ends (85 seconds), analyze the output file of the queue monitoring script (router.txt) to find the average queue length:

		cat router.txt | sed 's/\p / /g' | awk  '{ sum += $37 } END { if (NR > 0) print sum / NR }'
		
3.1.9. Investigate the log file on the of trafic sink (server) to find the real mean packet arrival rate, number of arrived packets and number of bytes arrived:

		ITGDec receiver.log
		
Divide the number of received bytes to number of received packets to find the average packet size. Calculate the real service rate with this average packet size and the service rate of the tocken bucket filter which has been set to 1 Mbps.
After this step you have experimental utilization factor and mean queue size.

3.1.10. Repeat steps 3.1.5 to 3.1.9 for the mean arrival rates belong to {125, 150, 175, 200, 225}. For each arraival rate repeat the procedure at least 5 times and gather the data.
Also, do this for M/M/1 queue and M/D/1 sepatately. (There is only a tiny difference between M/M/1 and M/D/1 in step 3.1.6)

3.1.11. After gathering the data do a data analysis like what has been done in the attached data sheet.


** 3.2. On NS2:**

3.2.1. Install NS2 in any of the reserved VMs:

		sudo apt-get update # refresh local information about software repositories
		sudo apt-get install ns2 # install ns2 network simulator

3.2.2. Copy mm1.tcl and md1.tcl files to the VM from your local computer.

3.2.3. Open mm1.tcl (or md1.tcl) in nano text editor on the VM:

		nano mm1.tcl
		
3.2.4. Set the mean arrival rate to whatever you want and save the file and exit from nano. For example:

	   set lambda 125.0
	   
	   
3.2.5. Run the script with an argument (rep) which correspond to the procedure of initializing the seeds:

M/M/1 case: 

		ns mm1.tcl rep
		
M/D/1 case: 

		ns md1.tcl rep
		
3.2.6. After run ends, qm.out is generated which contains the queue report (like in lab 3 instruction). We can extract the average queue length by running:

		cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }' 
		
3.2.7. Run the following command and read the last value which is printed on the screen. This value is total number of arrived packets.

		cat qm.out | awk  '{ print $6 }'
		
3.2.8. Run the following command and read the last value which is printed on the screen. This value is total number of arrived bytes.

		cat qm.out | awk  '{ print $9 }'
		
3.2.9. Divide number of received bytes by number of received packets to obtain the average packet size. Divide number of recieved packets by simulation time (which is set to 60 secons)
and find the actual mean arrival rate.

3.2.10. Repeat steps 3.2.4 to 3.2.9 for the mean arrival rates belong to {125, 150, 175, 200, 225}. For each arraival rate repeat the procedure at least 5 times and gather the data.
Also, do this for M/M/1 queue and M/D/1 sepatately. 

3.2.11. After gathering the data do a data analysis like what has been done in the attached data sheet.

		
		




	